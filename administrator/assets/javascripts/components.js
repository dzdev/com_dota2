// Javascript for components field
jQuery(document).ready(function(){
  var handler = function() {
    var $wrapper = jQuery(this).parents(".item-component-wrapper.last-wrapper"),
        $clone = $wrapper.clone(),
        index, counter, clone_html;

    // Remove handler because this wrapper isn't the last anymore
    $wrapper.find("select").off('change');
    $wrapper.removeClass("last-wrapper");

    // Remove old chosen container
    $clone.find(".chzn-container").remove();

    // Compute the index and counter
    var index = parseInt($clone.data('index'));
    var counter = index + 1;

    // Increase the index and counter from the clone
    clone_html = $clone.clone().wrap('<p>').parent().html();
    clone_html = clone_html.replace(new RegExp(counter, "g"), counter + 1);
    clone_html = clone_html.replace(new RegExp(index, "g"), index + 1);

    // Add the clone to DOM
    $clone = jQuery(clone_html);
    $clone = $clone.insertAfter($wrapper);

    // Set up chosen and tooltip
    $clone.find("select").val("").show().chosen().change(handler);
    $clone.find("label").tooltip();
  }

  jQuery(".item-component-wrapper.last-wrapper select").on('change', handler);
});