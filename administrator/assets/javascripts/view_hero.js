jQuery(document).ready(function(){
    // Binding for attribs field
    var attribs = eval('(' + jQuery('input[name="jform[attribs]"]').val() + ')');
    var updateAttribsInput = function() {jQuery('input[name="jform[attribs]"]').val(JSON.stringify(attribs))}
    jQuery('input[name="attribs[str][b]"]').val(attribs.str.b).on('change', function() { attribs.str.b = jQuery(this).val(); updateAttribsInput()});
    jQuery('input[name="attribs[str][g]"]').val(attribs.str.g).on('change', function() { attribs.str.g = jQuery(this).val(); updateAttribsInput()});
    jQuery('input[name="attribs[agi][b]"]').val(attribs.agi.b).on('change', function() { attribs.agi.b = jQuery(this).val(); updateAttribsInput()});
    jQuery('input[name="attribs[agi][g]"]').val(attribs.agi.g).on('change', function() { attribs.agi.g = jQuery(this).val(); updateAttribsInput()});
    jQuery('input[name="attribs[int][b]"]').val(attribs.int.b).on('change', function() { attribs.int.b = jQuery(this).val(); updateAttribsInput()});
    jQuery('input[name="attribs[int][g]"]').val(attribs.int.g).on('change', function() { attribs.int.g = jQuery(this).val(); updateAttribsInput()});
    jQuery('input[name="attribs[ms]"]').val(attribs.ms).on('change', function() { attribs.ms = jQuery(this).val(); updateAttribsInput()});
    jQuery('input[name="attribs[armor]"]').val(attribs.armor).on('change', function() { attribs.armor = jQuery(this).val(); updateAttribsInput()});
    jQuery('input[name="attribs[dmg][min]"]').val(attribs.dmg.min).on('change', function() { attribs.dmg.min = jQuery(this).val(); updateAttribsInput()});
    jQuery('input[name="attribs[dmg][max]"]').val(attribs.dmg.max).on('change', function() { attribs.dmg.max = jQuery(this).val(); updateAttribsInput()});
});
