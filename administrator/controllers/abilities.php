<?php
/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

/**
 * Abilities list controller class.
 */
class Dota2ControllerAbilities extends JControllerAdmin
{
    protected $option = 'com_dota2';
    
    /**
     * Proxy for getModel.
     * @since   1.6
     */
    public function getModel($name = 'ability', $prefix = 'Dota2Model')
    {
        $model = parent::getModel($name, $prefix, array('ignore_request' => true));
        return $model;
    }
    
    
    /**
     * Method to save the submitted ordering values for records via AJAX.
     *
     * @return  void
     *
     * @since   3.0
     */
    public function saveOrderAjax()
    {
        // Get the input
        $input = JFactory::getApplication()->input;
        $pks = $input->post->get('cid', array(), 'array');
        $order = $input->post->get('order', array(), 'array');

        // Sanitize the input
        JArrayHelper::toInteger($pks);
        JArrayHelper::toInteger($order);

        // Get the model
        $model = $this->getModel();

        // Save the ordering
        $return = $model->saveorder($pks, $order);

        if ($return)
        {
            echo "1";
        }

        // Close the application
        JFactory::getApplication()->close();
    }
    
    public function getAbilityList() {
        $http = JHttpFactory::getHttp();
        $response = $http->get('http://www.dota2.com/jsfeed/abilitydata?l=en');
        
        $json = json_decode($response->body, true);
        $data = $json['abilitydata'];
        
        header('Content-Type: application/json');
        echo json_encode($data);
        JFactory::getApplication()->close();
    }
    
    public function saveAbility() {
        JTable::addIncludePath(JPATH_COMPONENT.'/tables/');
        $table = JTable::getInstance('Ability', 'Dota2Table');
        
        $db = JFactory::getDbo();
        $db->setQuery("SELECT id FROM `#__usergroups`");
        $usergroups = $db->loadAssocList();
        $user = JFactory::getUser();
        
        $ability = JRequest::getVar('ability', null, 'post');
        $data = JRequest::getVar('data', null, 'post');
        $no_images = JRequest::getVar('no_images', false, 'post', 'boolean');
        $data['name'] = $ability;
        if ( $table->load( array('name'=> $ability) ) ) {
            $data['id'] = $table->id;
            
            // Preserve tags
            $tag_helper = new JHelperTags;
            $data['tags'] = explode(',', $tag_helper->getTagIds($table->id, 'com_dota2.ability'));
        } else {
            $data['id'] = 0;
            $data['created_by'] = $user->get('id');
            foreach($usergroups as $group) {
                $data['rules']['core.create'][$group['id']] = '';
                $data['rules']['core.delete'][$group['id']] = '';
                $data['rules']['core.edit'][$group['id']] = '';
                $data['rules']['core.edit.state'][$group['id']] = '';
                $data['rules']['core.edit.own'][$group['id']] = '';
            }
            $data['params'] = array(
                    'affects_alt' => '',
                    'attrib_alt'  => '',
                    'cmb_alt'     => '',
                    'desc_alt'    => '',
                    'dmg_alt'     => '',
                    'lore_alt'    => '',
                    'media'       => '',
                    'shortcut'    => '',
            );
        }
        
        header('Content-Type: application/json');
        $model = JModelLegacy::getInstance('Ability', 'Dota2Model');
        $form = $model->getForm($data, false);
        if (!$form) {
            header('HTTP/1.0 500');
            echo json_encode(array('status' => 'failure', 'message' => $model->getError()));
            JFactory::getApplication()->close();
        }
        
        $validData = $model->validate($form, $data);
        
        if (!$validData || !$model->save($validData)) {
            header('HTTP/1.0 500');
            echo json_encode(array('status' => 'failure', 'message' => $model->getError()));
            JFactory::getApplication()->close();
        }
        
        // Download images for abilities
        $http = JHttpFactory::getHttp();
        
        // Download dota 2 images
        $media_path = JPATH_ROOT . '/media/com_dota2/images/dota2/abilities';
        $have_dota2_folder = true;
        if (!JFolder::exists($media_path)) {
            if (!JFolder::create($media_path))
                $have_dota2_folder = false;
        }
        if ($have_dota2_folder && !$no_images) {
            foreach (array('hp1', 'hp2') as $size) {
                $response = $http->get("http://media.steampowered.com/apps/dota2/images/abilities/${ability}_${size}.png");
                if ($response->code == 200) {
                    JFile::write("${media_path}/${ability}_${size}.png", $response->body);
                }
            }
        }
        
        $media_path = JPATH_ROOT . '/media/com_dota2/images/dota1/abilities';
        $have_dota1_folder = true;
        if (!JFolder::exists($media_path)) {
            if (!JFolder::create($media_path))
                $have_dota1_folder = false;
        }
        if ($have_dota1_folder && !$no_images) {
            foreach (array('hp1') as $size) {
                $response = $http->get("http://dzdev.bitbucket.org/repository/dota1/abilities/${ability}_${size}.png");
                if ($response->code == 200) {
                    JFile::write("${media_path}/${ability}_${size}.png", $response->body);
                }
            }
        }
        
        if ($have_dota1_folder && $have_dota2_folder || $no_images) {
            echo json_encode(array('status' => 'ok'));
        } else {
            header('HTTP/1.0 500');
            echo json_encode(array('status' => 'failure', 'message' => 'Can\'t create image folder(s) for abilities'));
        }
        JFactory::getApplication()->close();
    }
}