<?php
/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

/**
 * Heroes list controller class.
 */
class Dota2ControllerHeroes extends JControllerAdmin
{
    /**
     * Proxy for getModel.
     * @since   1.6
     */
    public function getModel($name = 'hero', $prefix = 'Dota2Model')
    {
        $model = parent::getModel($name, $prefix, array('ignore_request' => true));
        return $model;
    }


    /**
     * Method to save the submitted ordering values for records via AJAX.
     *
     * @return  void
     *
     * @since   3.0
     */
    public function saveOrderAjax()
    {
        // Get the input
        $input = JFactory::getApplication()->input;
        $pks = $input->post->get('cid', array(), 'array');
        $order = $input->post->get('order', array(), 'array');

        // Sanitize the input
        JArrayHelper::toInteger($pks);
        JArrayHelper::toInteger($order);

        // Get the model
        $model = $this->getModel();

        // Save the ordering
        $return = $model->saveorder($pks, $order);

        if ($return)
        {
            echo "1";
        }

        // Close the application
        JFactory::getApplication()->close();
    }
    
    public function getHeroList() {
        // Get heroesdata
        $http = JHttpFactory::getHttp();
        
        $response = $http->get('http://www.dota2.com/jsfeed/heropediadata?feeds=herodata');
        $heropediadata = json_decode($response->body, true);
        $heropediadata = $heropediadata['herodata'];

        $response = $http->get('http://www.dota2.com/jsfeed/heropickerdata');
        $heropickerdata = json_decode($response->body, true);

        $response = $http->get('http://api.herostats.io/heroes/all');
        $heroAttribsData = json_decode($response->body, true);
        $preparedAttribs = array();
        
        // Name mapping between 2 APIs
        $nameMap = array(
                "Abbaddon" => "Abaddon",
                "Windrunner" => "Windranger",
                "Necrolyte" => "Necrophos",
                "Skeleton King" => "Wraith King",
                "Lycanthrope" => "Lycan",
                "Wisp" => "Io"
        );
        
        // Prepare attribs
        foreach ($heroAttribsData as $hero) {
            $name = array_key_exists($hero["Name"], $nameMap) ? $nameMap[$hero["Name"]] : $hero["Name"];
            $preparedAttribs[$name] = $hero;
        }
        
        foreach ($heropediadata as $name => &$value) {
            $value = array_merge($value, $heropickerdata[$name]);
            // JankDota Heroes API may not have latest heroes
            if (empty($preparedAttribs[$value["dname"]]))
                continue;
            $value["attribs"]["range"] = $preparedAttribs[$value["dname"]]["Range"];
            $value["attribs"]["base_attack_time"] = $preparedAttribs[$value["dname"]]["BaseAttackTime"];
            $value["attribs"]["day_vision"] = $preparedAttribs[$value["dname"]]["DayVision"];
            $value["attribs"]["night_vision"] = $preparedAttribs[$value["dname"]]["NightVision"];
            if ($preparedAttribs[$value["dname"]]["Alignment"] === 0) {
                $value["faction"] = "radiant";
            } else if ($preparedAttribs[$value["dname"]]["Alignment"] === 1) {
                $value["faction"] = "dire";
            }
        }
        
        header('Content-Type: application/json');
        echo json_encode($heropediadata);
        JFactory::getApplication()->close();
    }
    
    public function saveHero() {
        $hero = JRequest::getVar('hero', null, 'post');
        $data = JRequest::getVar('data', null, 'post');
        $no_images = JRequest::getVar('no_images', false, 'post', 'boolean');
        $data['name'] = $hero;
        
        JTable::addIncludePath(JPATH_COMPONENT.'/tables/');
        $table = JTable::getInstance('Hero', 'Dota2Table');
        
        $db = JFactory::getDbo();
        $db->setQuery("SELECT id FROM `#__usergroups`");
        $usergroups = $db->loadAssocList();
        $user = JFactory::getUser();
        
        if ($table->load( array('name' => $hero)) ) {
             $data['id'] = $table->id;
             
            // Preserve tags
            $tag_helper = new JHelperTags;
            $data['tags'] = explode(',', $tag_helper->getTagIds($table->id, 'com_dota2.hero'));
        } else {
            // Add default value for fields not existing in the data
            $data['id'] = 0;
            $data['created_by'] = $user->get('id');
            foreach($usergroups as $group) {
                $data['rules']['core.create'][$group['id']] = '';
                $data['rules']['core.delete'][$group['id']] = '';
                $data['rules']['core.edit'][$group['id']] = '';
                $data['rules']['core.edit.state'][$group['id']] = '';
                $data['rules']['core.edit.own'][$group['id']] = '';
            }
            $data['params'] = array(
                    'bio_alt' => '',
                    'rating1' => 0,
                    'rating2' => 0,
                    'rating3' => 0,
                    'rating4' => 0,
                    'guide'   => '',
                    'artwork_dir' => '');
        }
        
        $data['attribs'] = json_encode($data['attribs']);

        header('Content-Type: application/json');
        $model = parent::getModel('Hero', 'Dota2Model');
        $form = $model->getForm($data, false);
        if (!$form) {
            header('HTTP/1.0 500');
            echo json_encode(array('status' => 500, 'message' => $model->getError()));
            JFactory::getApplication()->close();
        }
        
        $validData = $model->validate($form, $data);
        if (!$validData) {
            header('HTTP/1.0 500');
            echo json_encode(array('status' => 500, 'message' => $model->getError()));
            JFactory::getApplication()->close();
        }
        
        if (!$this->allowSave($validData, $table->getKeyName()) 
            || !$model->save($validData)
        ) {
            header('HTTP/1.0 500');
            echo json_encode(array('status' => 500, 'message' => $model->getError()));
            JFactory::getApplication()->close();
        }
        
        // Download images for heroes
        $http = JHttpFactory::getHttp();
        
        // Download dota 2 images
        $media_path = JPATH_ROOT . '/media/com_dota2/images/dota2/heroes';
        $have_dota2_folder = true;
        if (!JFolder::exists($media_path)) {
            if (!JFolder::create($media_path))
                $have_dota2_folder = false;
        }
        if ($have_dota2_folder && !$no_images) {
            foreach (array('hphover', 'sb', 'full') as $size) {
                $response = $http->get("http://media.steampowered.com/apps/dota2/images/heroes/${hero}_${size}.png");
                if ($response->code == 200) {
                    JFile::write("${media_path}/${hero}_${size}.png", $response->body);
                }
            }
            $response = $http->get("http://media.steampowered.com/apps/dota2/images/heroes/${hero}_vert.jpg");
            if ($response->code == 200) {
                JFile::write("${media_path}/${hero}_vert.jpg", $response->body);
            }
        }
        
        // Download dota 1 images
        $media_path = JPATH_ROOT . '/media/com_dota2/images/dota1/heroes';
        $have_dota1_folder = true;
        if (!JFolder::exists($media_path)) {
            if (!JFolder::create($media_path))
                $have_dota1_folder = false;
        }
        if ($have_dota1_folder && !$no_images) {
            foreach (array('hphover', 'full') as $size) {
                $response = $http->get("http://dzdev.bitbucket.org/repository/dota1/heroes/${hero}_${size}.png");
                if ($response->code == 200) {
                    JFile::write("${media_path}/${hero}_${size}.png", $response->body);
                }
            }
        }
        
        if ($have_dota1_folder && $have_dota2_folder || $no_images) {
            echo json_encode(array('status' => 'ok'));
        } else {
            header('HTTP/1.0 500');
            echo json_encode(array('status' => 'failure', 'message' => 'Can\'t create image folder(s) for heroes'));
        }
        JFactory::getApplication()->close();
    }

    /**
     * Method to check if you can add a new record.
     *
     * Extended classes can override this if necessary.
     *
     * @param   array  $data  An array of input data.
     *
     * @return  boolean
     *
     * @since   12.2
     */
    protected function allowAdd($data = array())
    {
        $user = JFactory::getUser();
        return ($user->authorise('core.create', $this->option) || count($user->getAuthorisedCategories($this->option, 'core.create')));
    }

    /**
     * Method to check if you can add a new record.
     *
     * Extended classes can override this if necessary.
     *
     * @param   array   $data  An array of input data.
     * @param   string  $key   The name of the key for the primary key; default is id.
     *
     * @return  boolean
     *
     * @since   12.2
     */
    protected function allowEdit($data = array(), $key = 'id')
    {
        return JFactory::getUser()->authorise('core.edit', $this->option);
    }

    /**
     * Method to check if you can save a new or existing record.
     *
     * Extended classes can override this if necessary.
     *
     * @param   array   $data  An array of input data.
     * @param   string  $key   The name of the key for the primary key.
     *
     * @return  boolean
     *
     * @since   12.2
     */
    protected function allowSave($data, $key = 'id')
    {
        $recordId = isset($data[$key]) ? $data[$key] : '0';

        if ($recordId)
        {
            return $this->allowEdit($data, $key);
        }
        else
        {
            return $this->allowAdd($data);
        }
    }
}