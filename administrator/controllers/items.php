<?php
/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

/**
 * Items list controller class.
 */
class Dota2ControllerItems extends JControllerAdmin
{
    /**
     * Proxy for getModel.
     * @since   1.6
     */
    public function getModel($name = 'item', $prefix = 'Dota2Model')
    {
        $model = parent::getModel($name, $prefix, array('ignore_request' => true));
        return $model;
    }


    /**
     * Method to save the submitted ordering values for records via AJAX.
     *
     * @return  void
     *
     * @since   3.0
     */
    public function saveOrderAjax()
    {
        // Get the input
        $input = JFactory::getApplication()->input;
        $pks = $input->post->get('cid', array(), 'array');
        $order = $input->post->get('order', array(), 'array');

        // Sanitize the input
        JArrayHelper::toInteger($pks);
        JArrayHelper::toInteger($order);

        // Get the model
        $model = $this->getModel();

        // Save the ordering
        $return = $model->saveorder($pks, $order);

        if ($return)
        {
            echo "1";
        }

        // Close the application
        JFactory::getApplication()->close();
    }

    public function getItemList() 
    {
        // Get itemdata
        $http = JHttpFactory::getHttp();
        $response = $http->get('http://www.dota2.com/jsfeed/itemdata?l=en');
        $data = json_decode($response->body, true);
        $items = $data['itemdata'];
        
        header('Content-Type: application/json');
        echo json_encode($items);
        JFactory::getApplication()->close();
    }
    
    public function saveItem()
    {
        JTable::addIncludePath(JPATH_COMPONENT.'/tables/');
        $table = JTable::getInstance('Item', 'Dota2Table');
        
        $db = JFactory::getDbo();
        $db->setQuery("SELECT id FROM `#__usergroups`");
        $usergroups = $db->loadAssocList();
        $user = JFactory::getUser();
        
        $item = JRequest::getVar('item', null, 'post');
        $data = JRequest::getVar('data', null, 'post');
        $no_images = JRequest::getVar('no_images', false, 'post', 'boolean');
        $data['name'] = $item;
        $data['dota_id'] = $data['id'];
        
        if ( $table->load( array('name'=> $item) ) ) {
            $data['id'] = $table->id;
            
            // Preserve tags
            $tag_helper = new JHelperTags;
            $data['tags'] = explode(',', $tag_helper->getTagIds($table->id, 'com_dota2.item'));
        } else {
            $data['id'] = 0;
            $data['created_by'] = $user->get('id');
            foreach($usergroups as $group) {
                $data['rules']['core.create'][$group['id']] = '';
                $data['rules']['core.delete'][$group['id']] = '';
                $data['rules']['core.edit'][$group['id']] = '';
                $data['rules']['core.edit.state'][$group['id']] = '';
                $data['rules']['core.edit.own'][$group['id']] = '';
            }
            $data['params'] = array(
                    'attrib_alt' => '',
                    'desc_alt'   => '',
                    'lore_alt'   => ''
            );
        }
        
        if ($data['created'])
            $data['created'] = 1;
        else
            $data['created'] = 0;
        
        header('Content-Type: application/json');
        $model = parent::getModel('Item', 'Dota2Model');
        $form = $model->getForm($data, false);
        if (!$form) {
            header('HTTP/1.0 500');
            echo json_encode(array('status' => 'failure', 'message' => $model->getError()));
            JFactory::getApplication()->close();
        }
        
        $validData = $model->validate($form, $data);
        
        if (!$validData || !$model->save($validData)) {
            header('HTTP/1.0 500');
            echo json_encode(array('status' => 'failure', 'message' => $model->getError()));
            JFactory::getApplication()->close();
        }
        
        // Download images for items
        $http = JHttpFactory::getHttp();
        
        // Download dota 2 images
        $media_path = JPATH_ROOT . '/media/com_dota2/images/dota2/items';
        $have_dota2_folder = true;
        if (!JFolder::exists($media_path)) {
            if (!JFolder::create($media_path))
                $have_dota2_folder = false;
        }
        if ($have_dota2_folder && !$no_images) {
            $response = $http->get("http://media.steampowered.com/apps/dota2/images/items/${item}_lg.png");
            if ($response->code == 200) {
                JFile::write("${media_path}/${item}_lg.png", $response->body);
            }
        }
        
        $media_path = JPATH_ROOT . '/media/com_dota2/images/dota1/items';
        $have_dota1_folder = true;
        if (!JFolder::exists($media_path)) {
            if (!JFolder::create($media_path))
                $have_dota1_folder = false;
        }
        if ($have_dota1_folder && !$no_images) {
            $response = $http->get("http://dzdev.bitbucket.org/repository/dota1/items/${item}_lg.png");
            if ($response->code == 200) {
                JFile::write("${media_path}/${item}_lg.png", $response->body);
            }
        }
        
        if ($have_dota1_folder && $have_dota2_folder || $no_images) {
            echo json_encode(array('status' => 'ok'));
        } else {
            header('HTTP/1.0 500');
            echo json_encode(array('status' => 'failure', 'message' => 'Can\'t create image folder(s) for items'));
        }
        JFactory::getApplication()->close();
    }
}