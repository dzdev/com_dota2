<?php
/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */

// No direct access
defined('_JEXEC') or die;

/**
 * Dota2 helper.
 */
class Dota2Helper
{
    /**
     * Configure the Linkbar.
     */
    public static function addSubmenu($vName = '')
    {
        JHtmlSidebar::addEntry(
            JText::_('COM_DOTA2_TITLE_ABILITIES'),
            'index.php?option=com_dota2&view=abilities',
            $vName == 'abilities'
        );
        JHtmlSidebar::addEntry(
            JText::_('COM_DOTA2_TITLE_ITEMS'),
            'index.php?option=com_dota2&view=items',
            $vName == 'items'
        );
        JHtmlSidebar::addEntry(
            JText::_('COM_DOTA2_TITLE_HEROES'),
            'index.php?option=com_dota2&view=heroes',
            $vName == 'heroes'
        );

    }

    /**
     * Gets a list of the actions that can be performed.
     *
     * @return  JObject
     * @since   1.6
     */
    public static function getActions()
    {
        $user   = JFactory::getUser();
        $result = new JObject;

        $assetName = 'com_dota2';

        $actions = array(
            'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
        );

        foreach ($actions as $action) {
            $result->set($action, $user->authorise($action, $assetName));
        }

        return $result;
    }
}
