<?php
/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */

defined('_JEXEC') or die;

abstract class JHtmlDota2
{
    /**
     * cached array of item components
     */
    protected static $item_components = null;
    protected static $heroes = null;

    public static function atks()
    {
        $options = array();
        $options[0] = new stdClass();
        $options[0]->value = "melee";
        $options[0]->text = "Melee";
        $options[1] = new stdClass();
        $options[1]->value = "ranged";
        $options[1]->text = "Ranged";

        return $options;
    }

    public static function faction()
    {
        $options = array();
        $options[0] = new stdClass();
        $options[0]->value = "radiant";
        $options[0]->text = "Radiant";
        $options[1] = new stdClass();
        $options[1]->value = "dire";
        $options[1]->text = "Dire";

        return $options;
    }

    public static function pas()
    {
        $options = array();
        $options[0] = new stdClass();
        $options[0]->value = "int";
        $options[0]->text = "Intelligence";
        $options[1] = new stdClass();
        $options[1]->value = "agi";
        $options[1]->text = "Agility";
        $options[2] = new stdClass();
        $options[2]->value = "str";
        $options[2]->text = "Strength";

        return $options;
    }

    public static function roles()
    {
        $options = array();
        $options[0] = new stdClass();
        $options[0]->value = "Disabler";
        $options[0]->text = "Disabler";
        $options[1] = new stdClass();
        $options[1]->value = "Nuker";
        $options[1]->text = "Nuker";
        $options[2] = new stdClass();
        $options[2]->value = "Support";
        $options[2]->text = "Support";
        $options[3] = new stdClass();
        $options[3]->value = "Escape";
        $options[3]->text = "Escape";
        $options[4] = new stdClass();
        $options[4]->value = "Initiator";
        $options[4]->text = "Initiator";
        $options[5] = new stdClass();
        $options[5]->value = "LaneSupport";
        $options[5]->text = "LaneSupport";
        $options[6] = new stdClass();
        $options[6]->value = "Durable";
        $options[6]->text = "Durable";
        $options[7] = new stdClass();
        $options[7]->value = "Carry";
        $options[7]->text = "Carry";
        $options[8] = new stdClass();
        $options[8]->value = "Pusher";
        $options[8]->text = "Pusher";
        $options[9] = new stdClass();
        $options[9]->value = "Jungler";
        $options[9]->text = "Jungler";

        return $options;
    }

    public static function components()
    {
        if (empty(self::$item_components)) {
            //Filter for the field ".components;
            jimport('joomla.form.form');
            $options = array();
            JForm::addFormPath(JPATH_COMPONENT_ADMINISTRATOR . '/models/forms');
            $form = JForm::getInstance('com_dota2.item', 'item');

            $field = $form->getField('filter_components');

            $query = $form->getFieldAttribute('filter_components','query');
            $translate = $form->getFieldAttribute('filter_components','translate');
            $key = $form->getFieldAttribute('filter_components','key_field');
            $value = $form->getFieldAttribute('filter_components','value_field');

            // Get the database object.
            $db = JFactory::getDBO();

            // Set the query and get the result list.
            $db->setQuery($query);
            $items = $db->loadObjectlist();

            // Build the field options.
            if (!empty($items))
            {
                foreach ($items as $item)
                {
                    if ($translate == true)
                    {
                        $options[] = JHtml::_('select.option', $item->$key, JText::_($item->$value));
                    }
                    else
                    {
                        $options[] = JHtml::_('select.option', $item->$key, $item->$value);
                    }
                }
            }
            self::$item_components = $options;
        }

        return self::$item_components;
    }

    public static function heroes()
    {
        if (empty(self::$heroes)) {
            $db = JFactory::getDBO();
            $query = $db->getQuery(true);

            $query->select('name, dname')
                  ->from('#__dota2_heroes')
                  ->order('dname ASC');
            $db->setQuery($query);
            $heroes = $db->loadObjectlist();

            $options = array();

            foreach($heroes as $hero) {
                $options[] = JHtml::_('select.option', $hero->name, $hero->dname);
            }
            self::$heroes = $options;
        }

        return self::$heroes;
    }

    public static function boolean()
    {
        $options = array();
        $options[0] = new stdClass();
        $options[0]->value = 0;
        $options[0]->text = JText::_('JNO');
        $options[1] = new stdClass();
        $options[1]->value = 1;
        $options[1]->text = JText::_('JYES');

        return $options;
    }

    public static function qualities()
    {
                $options = array();
        $options[0] = new stdClass();
        $options[0]->value = "artifact";
        $options[0]->text = "Artifact";
        $options[1] = new stdClass();
        $options[1]->value = "common";
        $options[1]->text = "Common";
        $options[2] = new stdClass();
        $options[2]->value = "component";
        $options[2]->text = "Component";
        $options[3] = new stdClass();
        $options[3]->value = "consumable";
        $options[3]->text = "Consumable";
        $options[4] = new stdClass();
        $options[4]->value = "epic";
        $options[4]->text = "Epic";
        $options[5] = new stdClass();
        $options[5]->value = "rare";
        $options[5]->text = "Rare";
        $options[6] = new stdClass();
        $options[6]->value = "secret_shop";
        $options[6]->text = "Secret Shop";

        return $options;
    }
}
