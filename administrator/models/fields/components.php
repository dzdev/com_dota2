<?php
/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */

defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');

/**
 * Supports an HTML select list of categories
 */
class JFormFieldComponents extends JFormField
{
    /**
     * The form field type.
     *
     * @var     string
     * @since   1.6
     */
    protected $type = 'components';

    /**
     * Method to get the field input markup.
     *
     * @return  string  The field input markup.
     * @since   1.6
     */
    protected function getInput()
    {
        $document = JFactory::getDocument();
        $document->addScript('components/com_dota2/assets/javascripts/components.js');

        $values = explode(',', $this->value);
        $html = array();

        // Initialize some field attributes.
        $attr = !empty($this->class) ? ' class="' . $this->class . ' item-component"' : 'class="item-component"';

        // Initialize JavaScript field attributes.
        $attr .= $this->onchange ? ' onchange="' . $this->onchange . '"' : '';

        // Get the field options.
        $options = (array) $this->getOptions();

        $html[] = "<input type='hidden' name='" . $this->name . "' value='' />";
        foreach ($values as $index => $value) {
            $input_html = JHtml::_('select.genericlist', $options, $this->name . "[$index]", trim($attr), 'value', 'text', $value, $this->id . "_" . $index);
            $html[] = "<div class='control-group item-component-wrapper' data-index='$index'>" .
                          "<div class='control-label'>" . $this->getLabel($index + 1) . "</div>" .
                          "<div class='controls'>" . $input_html . "</div>" .
                      "</div>";
        }

        $index++;
        $input_html = JHtml::_('select.genericlist', $options, $this->name . "[$index]", trim($attr), 'value', 'text', NULL, $this->id . "_" . $index);
        $html[] = "<div class='control-group item-component-wrapper last-wrapper' data-index='$index'>" .
                      "<div class='control-label'>" . $this->getLabel($index + 1) . "</div>" .
                      "<div class='controls'>" . $input_html . "</div>" .
                  "</div>";

        return join('', $html);
    }

    /**
     * Method to get the field label markup.
     *
     * @return  string  The field label markup.
     *
     * @since   11.1
     */
    protected function getLabel($index = null)
    {
      $label = '';

      if ($this->hidden)
      {
        return $label;
      }

      // Get the label text from the XML element, defaulting to the element name.
      $text = $this->element['label'] ? (string) $this->element['label'] : (string) $this->element['name'];
      $text = $this->translateLabel ? JText::_($text) : $text;
      $text = $index ? $text . " " . $index : $text;

      // Build the class for the label.
      $class = !empty($this->description) ? 'hasTooltip' : '';
      $class = $this->required == true ? $class . ' required' : $class;
      $class = !empty($this->labelclass) ? $class . ' ' . $this->labelclass : $class;

      // Add the opening label tag and main attributes attributes.
      $label .= '<label id="' . $this->id . '-lbl" for="' . $this->id . '" class="' . $class . '"';

      // If a description is specified, use it to build a tooltip.
      if (!empty($this->description))
      {
        // Don't translate discription if specified in the field xml.
        $description = $this->translateDescription ? JText::_($this->description) : $this->description;
        JHtml::_('bootstrap.tooltip');
        $label .= ' title="' . JHtml::tooltipText(trim($text, ':'), $description, 0) . '"';
      }

      // Add the label text and closing tag.
      if ($this->required)
      {
        $label .= '>' . $text . '<span class="star">&#160;*</span></label>';
      }
      else
      {
        $label .= '>' . $text . '</label>';
      }

      return $label;
    }

    /**
    * Method to get the custom field options.
    * Use the query attribute to supply a query to generate the list.
    *
    * @return  array  The field option objects.
    *
    * @since   11.1
    */
    protected function getOptions()
    {
      $options = array();

      // Initialize some field attributes.
      $key   = 'name';
      $value = 'dname';

      // Get the database object.
      $db = JFactory::getDbo();

      // Set the query and get the result list.
      $db->setQuery("SELECT NULL AS `name`, '- Select Components -' AS `dname` UNION SELECT `name`, `dname` FROM `#__dota2_items`");
      $items = $db->loadObjectlist();

      // Build the field options.
      if (!empty($items))
      {
        foreach ($items as $item)
        {
          if ($this->translate == true)
          {
            $options[] = JHtml::_('select.option', $item->$key, JText::_($item->$value));
          }
          else
          {
            $options[] = JHtml::_('select.option', $item->$key, $item->$value);
          }
        }
      }

      return $options;
    }
}