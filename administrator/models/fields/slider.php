<?php
/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */

defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');

/**
 * Supports an HTML select list of categories
 */
class JFormFieldSlider extends JFormField
{
    /**
     * The form field type.
     *
     * @var     string
     * @since   1.6
     */
    protected $type = 'slider';

    /**
     * Method to get the field input markup.
     *
     * @return  string  The field input markup.
     * @since   1.6
     */
    protected function getInput()
    {
        $name = $this->element['name'];
        
        $html  = "<fieldset>";
        $html .= "<legend>" . JText::_($this->element['label']) .  " (<span id='$name-info'>" . $this->value . "</span>)</legend>";
        $html .= "<input 
                    type='text' name='" . $this->name . "' id='$name-slider' 
                    value='" . $this->value . "'
                    data-slider-value='" . $this->value . "'
                    data-slider-min='". $this->element['first'] . "' 
                    data-slider-max='". $this->element['last'] . "' 
                    data-slider-step='". $this->element['step'] . "' 
                    />";
        $html .= "</fieldset>";
        
        $script = "
            jQuery(document).ready(function(){
                var \$input = jQuery('#$name-slider');
                var \$info = jQuery('#$name-info');
                \$input.slider().on('slide', function(ev){  
                    \$input.val(ev.value);
                    \$info.text(ev.value);
                });
            });
        ";
        $document = JFactory::getDocument();
        $document->addScript('components/com_dota2/assets/javascripts/bootstrap-slider.js');
        $document->addStyleSheet('components/com_dota2/assets/css/slider.css');
        $document->addScriptDeclaration($script);
        return $html;
    }
}