<?php

/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Dota2 records.
 */
class Dota2Modelheroes extends JModelList {

    /**
     * Constructor.
     *
     * @param    array    An optional associative array of configuration settings.
     * @see        JController
     * @since    1.6
     */
    public function __construct($config = array()) {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                                'id', 'a.id',
                'ordering', 'a.ordering',
                'state', 'a.state',
                'created_by', 'a.created_by',
                'name', 'a.name',
                'dname', 'a.dname',
                'title', 'a.title',
                'faction', 'a.faction',
                'atk', 'a.atk',
                'bio', 'a.bio',
                'droles', 'a.droles',
                'pa', 'a.pa',
                'attribs', 'a.attribs',
                'metakey', 'a.metakey',
                'metadesc', 'a.metadesc',
                'metadata', 'a.metadata',
                'params', 'a.params',

            );
        }

        parent::__construct($config);
    }

    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     */
    protected function populateState($ordering = null, $direction = null) {
        // Initialise variables.
        $app = JFactory::getApplication('administrator');

        // Load the filter state.
        $search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);

        $published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
        $this->setState('filter.state', $published);


        //Filtering atk
        $this->setState('filter.atk', $app->getUserStateFromRequest($this->context.'.filter.atk', 'filter_atk', '', 'string'));

        //Filtering pa
        $this->setState('filter.pa', $app->getUserStateFromRequest($this->context.'.filter.pa', 'filter_pa', '', 'string'));

        //Filtering roles
        $this->setState('filter.roles', $app->getUserStateFromRequest($this->context.'.filter.roles', 'filter_roles', '', 'string'));

        //Filtering faction
        $this->setState('filter.faction', $app->getUserStateFromRequest($this->context.'.filter.faction', 'filter_faction', '', 'string'));

        // Load the parameters.
        $params = JComponentHelper::getParams('com_dota2');
        $this->setState('params', $params);

        // List state information.
        parent::populateState('a.name', 'asc');
    }

    /**
     * Method to get a store id based on model configuration state.
     *
     * This is necessary because the model is used by the component and
     * different modules that might need different sets of data or different
     * ordering requirements.
     *
     * @param   string      $id A prefix for the store id.
     * @return  string      A store id.
     * @since   1.6
     */
    protected function getStoreId($id = '') {
        // Compile the store id.
        $id.= ':' . $this->getState('filter.search');
        $id.= ':' . $this->getState('filter.state');

        return parent::getStoreId($id);
    }

    /**
     * Build an SQL query to load the list data.
     *
     * @return  JDatabaseQuery
     * @since   1.6
     */
    protected function getListQuery() {
        // Create a new query object.
        $db = $this->getDbo();
        $query = $db->getQuery(true);

        // Select the required fields from the table.
        $query->select(
                $this->getState(
                        'list.select', 'a.*'
                )
        );
        $query->from('`#__dota2_heroes` AS a');


    // Join over the users for the checked out user.
    $query->select('uc.name AS editor');
    $query->join('LEFT', '#__users AS uc ON uc.id=a.checked_out');

        // Join over the user field 'created_by'
        $query->select('created_by.name AS created_by');
        $query->join('LEFT', '#__users AS created_by ON created_by.id = a.created_by');


    // Filter by published state
    $published = $this->getState('filter.state');
    if (is_numeric($published)) {
        $query->where('a.state = '.(int) $published);
    } else if ($published === '') {
        $query->where('(a.state IN (0, 1))');
    }


        // Filter by search in title
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            if (stripos($search, 'id:') === 0) {
                $query->where('a.id = ' . (int) substr($search, 3));
            } else {
                $search = $db->Quote('%' . $db->escape($search, true) . '%');
                $query->where('( a.name LIKE '.$search.'  OR  a.dname LIKE '.$search.'  OR  a.droles LIKE '.$search.' )');
            }
        }



        //Filtering atk
        $filter_atk = $this->state->get("filter.atk");
        if ($filter_atk) {
            $query->where("a.atk = '".$db->escape($filter_atk)."'");
        }

        //Filtering pa
        $filter_pa = $this->state->get("filter.pa");
        if ($filter_pa != '') {
            $query->where("a.pa = '".$db->escape($filter_pa)."'");
        }

        //Filtering roles
        $filter_roles = $this->state->get("filter.roles");
        if ($filter_roles != '') {
            $query->where("a.droles LIKE '%".$db->escape($filter_roles)."%'");
        }

        // Filtering faction
        $filter_faction = $this->state->get("filter.faction");
        if ($filter_faction) {
            $query->where("a.faction = '" . $db->escape($filter_faction) . "'");
        }

        // Add the list ordering clause.
        $orderCol = $this->state->get('list.ordering');
        $orderDirn = $this->state->get('list.direction');
        if ($orderCol && $orderDirn) {
            $query->order($db->escape($orderCol . ' ' . $orderDirn));
        }

        return $query;
    }

    public function getItems() {
        $items = parent::getItems();

        return $items;
    }

}
