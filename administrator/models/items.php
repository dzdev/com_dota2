<?php

/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Dota2 records.
 */
class Dota2Modelitems extends JModelList {

    /**
     * Constructor.
     *
     * @param    array    An optional associative array of configuration settings.
     * @see        JController
     * @since    1.6
     */
    public function __construct($config = array()) {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                                'id', 'a.id',
                'ordering', 'a.ordering',
                'state', 'a.state',
                'created_by', 'a.created_by',
                'name', 'a.name',
                'attrib', 'a.attrib',
                'cd', 'a.cd',
                'dname', 'a.dname',
                'components', 'a.components',
                'cost', 'a.cost',
                'created', 'a.created',
                'desc', 'a.desc',
                'dota_id', 'a.dota_id',
                'lore', 'a.lore',
                'mc', 'a.mc',
                'qual', 'a.qual',
                'params', 'a.params',
                'metakey', 'a.metakey',
                'metadesc', 'a.metadesc',
                'metadata', 'a.metadata',

            );
        }

        parent::__construct($config);
    }

    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     */
    protected function populateState($ordering = null, $direction = null) {
        // Initialise variables.
        $app = JFactory::getApplication('administrator');

        // Load the filter state.
        $search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);

        $published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
        $this->setState('filter.state', $published);


        //Filtering components
        $this->setState('filter.components', $app->getUserStateFromRequest($this->context.'.filter.components', 'filter_components', '', 'string'));

        //Filtering created
        $this->setState('filter.created', $app->getUserStateFromRequest($this->context.'.filter.created', 'filter_created', '', 'string'));

        //Filtering qual
        $this->setState('filter.qual', $app->getUserStateFromRequest($this->context.'.filter.qual', 'filter_qual', '', 'string'));

        //Filtering metakey
        $this->setState('filter.metakey', $app->getUserStateFromRequest($this->context.'.filter.metakey', 'filter_metakey', '', 'string'));


        // Load the parameters.
        $params = JComponentHelper::getParams('com_dota2');
        $this->setState('params', $params);

        // List state information.
        parent::populateState('a.name', 'asc');
    }

    /**
     * Method to get a store id based on model configuration state.
     *
     * This is necessary because the model is used by the component and
     * different modules that might need different sets of data or different
     * ordering requirements.
     *
     * @param   string      $id A prefix for the store id.
     * @return  string      A store id.
     * @since   1.6
     */
    protected function getStoreId($id = '') {
        // Compile the store id.
        $id.= ':' . $this->getState('filter.search');
        $id.= ':' . $this->getState('filter.state');

        return parent::getStoreId($id);
    }

    /**
     * Build an SQL query to load the list data.
     *
     * @return  JDatabaseQuery
     * @since   1.6
     */
    protected function getListQuery() {
        // Create a new query object.
        $db = $this->getDbo();
        $query = $db->getQuery(true);

        // Select the required fields from the table.
        $query->select(
                $this->getState(
                        'list.select', 'a.*'
                )
        );
        $query->from('`#__dota2_items` AS a');


    // Join over the users for the checked out user.
    $query->select('uc.name AS editor');
    $query->join('LEFT', '#__users AS uc ON uc.id=a.checked_out');

        // Join over the user field 'created_by'
        $query->select('created_by.name AS created_by');
        $query->join('LEFT', '#__users AS created_by ON created_by.id = a.created_by');
        // Join over the foreign key 'components'
        $query->select('#__dota2_items_607633.dname AS items_dname_607633');
        $query->join('LEFT', '#__dota2_items AS #__dota2_items_607633 ON #__dota2_items_607633.name = a.components');


    // Filter by published state
    $published = $this->getState('filter.state');
    if (is_numeric($published)) {
        $query->where('a.state = '.(int) $published);
    } else if ($published === '') {
        $query->where('(a.state IN (0, 1))');
    }


        // Filter by search in title
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            if (stripos($search, 'id:') === 0) {
                $query->where('a.id = ' . (int) substr($search, 3));
            } else {
                $search = $db->Quote('%' . $db->escape($search, true) . '%');
                $query->where('( a.name LIKE '.$search.'  OR  a.dname LIKE '.$search.'  OR  a.dota_id LIKE '.$search.'  OR  a.lore LIKE '.$search.'  OR  a.qual LIKE '.$search.' )');
            }
        }



        //Filtering components
        $filter_components = $this->state->get("filter.components");
        if ($filter_components) {
            $query->where("a.components REGEXP '(^$filter_components,|,$filter_components,|,$filter_components$|^$filter_components$)'");
        }

        //Filtering created
        $filter_created = $this->state->get("filter.created");
        if ($filter_created != '') {
            $query->where("a.created = '".$db->escape($filter_created)."'");
        }

        //Filtering qual
        $filter_qual = $this->state->get("filter.qual");
        if ($filter_qual) {
            $query->where("a.qual = '".$db->escape($filter_qual)."'");
        }

        //Filtering metakey


        // Add the list ordering clause.
        $orderCol = $this->state->get('list.ordering');
        $orderDirn = $this->state->get('list.direction');
        if ($orderCol && $orderDirn) {
            $query->order($db->escape($orderCol . ' ' . $orderDirn));
        }

        return $query;
    }

    public function getItems() {
        $items = parent::getItems();

        foreach ($items as $oneItem) {

            if (isset($oneItem->components)) {
                $values = explode(',', $oneItem->components);

                $textValue = array();
                foreach ($values as $value){
                    $db = JFactory::getDbo();
                    $query = $db->getQuery(true);
                    $query
                            ->select('dname')
                            ->from('`#__dota2_items`')
                            ->where('name = \'' .$value.'\'');
                    $db->setQuery($query);
                    $results = $db->loadObject();
                    if ($results) {
                        $textValue[] = $results->dname;
                    }
                }

            $oneItem->components = !empty($textValue) ? implode(', ', $textValue) : $oneItem->components;

            }
        }
        return $items;
    }

}
