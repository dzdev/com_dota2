CREATE TABLE IF NOT EXISTS `#__dota2_abilities` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`asset_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT '1',
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`created_by` INT(11)  NOT NULL ,
`name` VARCHAR(255)  NOT NULL ,
`dname` VARCHAR(255)  NOT NULL ,
`affects` TEXT NOT NULL ,
`attrib` TEXT NOT NULL ,
`cmb` TEXT NOT NULL ,
`desc` TEXT NOT NULL ,
`dmg` TEXT NOT NULL ,
`hurl` VARCHAR(255)  NOT NULL ,
`lore` TEXT NOT NULL ,
`params` TEXT NOT NULL ,
`metakey` TEXT NOT NULL ,
`metadesc` TEXT NOT NULL ,
`metadata` TEXT NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

CREATE TABLE IF NOT EXISTS `#__dota2_items` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`asset_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT '1',
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`created_by` INT(11)  NOT NULL ,
`name` VARCHAR(255)  NOT NULL ,
`attrib` TEXT NOT NULL ,
`cd` VARCHAR(255)  NOT NULL ,
`dname` VARCHAR(255)  NOT NULL ,
`components` TEXT NOT NULL ,
`cost` INT(11)  NOT NULL ,
`created` VARCHAR(255)  NOT NULL ,
`desc` TEXT NOT NULL ,
`dota_id` INT(11)  NOT NULL ,
`lore` TEXT NOT NULL ,
`mc` INT(11)  NOT NULL ,
`qual` VARCHAR(255)  NOT NULL ,
`params` TEXT NOT NULL ,
`metakey` TEXT NOT NULL ,
`metadesc` TEXT NOT NULL ,
`metadata` TEXT NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

CREATE TABLE IF NOT EXISTS `#__dota2_heroes` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`asset_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL DEFAULT '1',
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`created_by` INT(11)  NOT NULL ,
`name` VARCHAR(255)  NOT NULL ,
`dname` VARCHAR(255)  NOT NULL ,
`title` TEXT NOT NULL,
`faction` TEXT NOT NULL,
`house` TEXT NOT NULL,
`atk` VARCHAR(255)  NOT NULL ,
`bio` TEXT NOT NULL ,
`droles` TEXT NOT NULL ,
`pa` VARCHAR(255)  NOT NULL ,
`attribs` TEXT NOT NULL ,
`metakey` TEXT NOT NULL ,
`metadesc` TEXT NOT NULL ,
`metadata` TEXT NOT NULL ,
`params` TEXT NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

INSERT INTO `#__content_types` (`type_id`, `type_title`, `type_alias`, `table`, `rules`, `field_mappings`, `router`) VALUES (NULL, 'Dota2 Hero', 'com_dota2.hero', '{"special":{"dbtable":"#__dota2_heroes","key":"id","type":"Hero","prefix":"Dota2Table","config":"array()"}}', '', '{"common":[{"core_content_item_id":"id","core_title":"dname","core_state":"state","core_alias":"name","core_body":"bio", "core_params":"params", "core_metadata":"metadata", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "asset_id":"asset_id"}]}', 'Dota2HelperRoute::getHeroRoute');
INSERT INTO `#__content_types` (`type_id`, `type_title`, `type_alias`, `table`, `rules`, `field_mappings`, `router`) VALUES (NULL, 'Dota2 Item', 'com_dota2.item', '{"special":{"dbtable":"#__dota2_items","key":"id","type":"Item","prefix":"Dota2Table","config":"array()"}}', '', '{"common":[{"core_content_item_id":"id","core_title":"dname","core_state":"state","core_alias":"name","core_body":"lore", "core_params":"params", "core_metadata":"metadata", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "asset_id":"asset_id"}]}', 'Dota2HelperRoute::getItemRoute');
INSERT INTO `#__content_types` (`type_id`, `type_title`, `type_alias`, `table`, `rules`, `field_mappings`, `router`) VALUES (NULL, 'Dota2 Ability', 'com_dota2.ability', '{"special":{"dbtable":"#__dota2_abilities","key":"id","type":"Ability","prefix":"Dota2Table","config":"array()"}}', '', '{"common":[{"core_content_item_id":"id","core_title":"dname","core_state":"state","core_alias":"name","core_body":"lore", "core_params":"params", "core_metadata":"metadata", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "asset_id":"asset_id"}]}', 'Dota2HelperRoute::getAbilityRoute');