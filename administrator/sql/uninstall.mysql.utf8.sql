DROP TABLE IF EXISTS `#__dota2_abilities`;
DROP TABLE IF EXISTS `#__dota2_items`;
DROP TABLE IF EXISTS `#__dota2_heroes`;

DELETE FROM `#__content_types` WHERE type_alias = 'com_dota2.hero';
DELETE FROM `#__content_types` WHERE type_alias = 'com_dota2.item';
DELETE FROM `#__content_types` WHERE type_alias = 'com_dota2.ability';