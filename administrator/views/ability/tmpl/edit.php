<?php
/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_dota2/assets/css/dota2.css');

$alt_language_fieldset = $this->form->getFieldset('alt_language');
$additional_fieldset = $this->form->getFieldset('additional');
?>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function(){

    });

    Joomla.submitbutton = function(task)
    {
        if(task == 'ability.cancel'){
            Joomla.submitform(task, document.getElementById('ability-form'));
        }
        else{

            if (task != 'ability.cancel' && document.formvalidator.isValid(document.id('ability-form'))) {

                Joomla.submitform(task, document.getElementById('ability-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>

<form action="<?php echo JRoute::_('index.php?option=com_dota2&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="ability-form" class="form-validate">
    <div class="form-inline form-inline-header">
        <div class="control-group">
            <div class="control-label"><?php echo $this->form->getLabel('dname'); ?></div>
            <div class="controls"><?php echo $this->form->getInput('dname'); ?></div>
        </div>
        <div class="control-group">
            <div class="control-label"><?php echo $this->form->getLabel('name'); ?></div>
            <div class="controls"><?php echo $this->form->getInput('name'); ?></div>
        </div>                    
    </div>
    <div class="row-fluid">
        <div class="span12 form-horizontal">
            <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general') ); ?>

            <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_DOTA2_ABILITY_DETAILS', true)); ?>
            <fieldset class="adminform">
            <div class="row-fluid">
                <div class="span6">
                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('hurl'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('hurl'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('tags'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('tags'); ?></div>
                    </div>
                </div>
                <div class="span6">
                    <div class="control-group">
                        <div class="control-label"><?php echo $additional_fieldset['jform_params_shortcut']->label; ?></div>
                        <div class="controls"><?php echo $additional_fieldset['jform_params_shortcut']->input; ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label"><?php echo $additional_fieldset['jform_params_media']->label; ?></div>
                        <div class="controls"><?php echo $additional_fieldset['jform_params_media']->input; ?></div>
                    </div>
                    
                </div>
            </div>
            <div class="row-fluid">
                <div class="span8">
                  <h1>Mô tả ngắn</h1>
                    <?php echo $alt_language_fieldset['jform_params_desc_alt']->input; ?>


                </div>
                <div class="span4">
                  <h1>Core Data</h1>
                  <div class="well">
                      <p><?php echo $this->item->desc; ?></p>
                        <p><?php echo $this->item->attrib; ?></p>
                        <p><?php echo $this->item->affects; ?></p>
                        <p><?php echo $this->item->cmb; ?></p>
                    </div>
                </div>
            </div>



            </fieldset>
            <?php echo JHtml::_('bootstrap.endTab'); ?>

            <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'publishing', JText::_('COM_DOTA2_FIELDSET_PUBLISHING')); ?>
            <fieldset class="adminform">
            <div class="control-group">
                <div class="control-label"><?php echo $this->form->getLabel('id'); ?></div>
                <div class="controls"><?php echo $this->form->getInput('id'); ?></div>
            </div>
            <div class="control-group">
                <div class="control-label"><?php echo $this->form->getLabel('state'); ?></div>
                <div class="controls"><?php echo $this->form->getInput('state'); ?></div>
            </div>
            <div class="control-group">
                <div class="control-label"><?php echo $this->form->getLabel('created_by'); ?></div>
                <div class="controls"><?php echo $this->form->getInput('created_by'); ?></div>
            </div>
            </fieldset>
            <?php echo JHtml::_('bootstrap.endTab'); ?>

            <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'metadata', JText::_('COM_DOTA2_FIELDSET_METADATA')); ?>
            <fieldset class="adminform">
                <?php echo JLayoutHelper::render('joomla.edit.metadata', $this); ?>
            </fieldset>
            <?php echo JHtml::_('bootstrap.endTab'); ?>

            <?php if (JFactory::getUser()->authorise('core.admin','dota2')): ?>
            <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'permissions', JText::_('COM_DOTA2_FIELDSET_RULES')); ?>
            <fieldset class="adminform">
                <?php echo JHtml::_('sliders.start', 'permissions-sliders-'.$this->item->id, array('useCookie'=>1)); ?>
                <?php echo JHtml::_('sliders.panel', JText::_('ACL Configuration'), 'access-rules'); ?>
                <?php echo $this->form->getInput('rules'); ?>
                <?php echo JHtml::_('sliders.end'); ?>
            </fieldset>
            <?php echo JHtml::_('bootstrap.endTab'); ?>
            <?php endif; ?>
        </div>
        <?php echo JHtml::_('bootstrap.endTabSet'); ?>
        <div class="clr"></div>

        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>

    </div>
</form>