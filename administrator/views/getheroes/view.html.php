<?php
/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');

/**
 * View class for a list of Dota2.
 */
class Dota2ViewGetHeroes extends JViewLegacy
{

    /**
     * Display the view
     */
    public function display($tpl = null)
    {
        $quick_mode = (boolean) JRequest::getVar('quick_mode');
        $this->assignRef('quick_mode', $quick_mode);
        
        parent::display($tpl);
    }
}
