<?php
/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */

// No direct access
defined('_JEXEC') or die;
?>
<div id="info" style="font-weight: bold; text-align: center">
</div>
<br />
<div id="progress" class="progress progress-striped active">
    <div class="bar" style="width: 100%"></div>
</div>
<div id="error">
</div>

<script type="text/javascript">
jQuery(document).ready(function() {
    var info = function(text) {
        jQuery("#info").html(text);
    };
    var progress = function(percent) {
        jQuery("#progress .bar").css('width', percent + '%');
    };
    var error = function(text) {
        jQuery("#error").append("<div class='alert alert-error'>" + text + "</div>");
    }
    // Get heroes list
    info("Retrieving item list...");
    jQuery.get("index.php?option=com_dota2&task=items.getItemList", function(data) {
        progress(0);
        info("Saving all items...");
        var items = data,
            names = Object.keys(items).sort(),
            total = names.length,
            index = 0, count = 0, error_count = 0,
            loop = function() { // Loop through entries, save one by one
                if (index >= total)
                    return;
                var name = names[index],
                    attribs = items[name];
                info("Saving item " + attribs.dname + "...");
                jQuery.ajax("index.php?option=com_dota2&task=items.saveItem", {
                    data: {item: name, data: attribs<?= ($this->quick_mode) ? ', no_images: true' : ''; ?>},
                    type: 'POST'
                })
                .done(function(data) {
                    count += 1;
                    progress( Math.floor((count + error_count) / total * 100) );

                    // Reload window when done
                    if (count == total) {
                        parent.location.reload();
                    } else if (count + error_count == total) {
                        jQuery('#progress').removeClass('active progress-striped');
                        info('Done with some errors!');
                    }
                })
                .fail(function(data) {
                    var response = data.responseJSON;
                    error("Failed to save " + attribs.dname + ": " + response.message);
                    
                    error_count += 1;
                    progress( Math.floor((count + error_count) / total * 100) );
                    if (count + error_count == total) {
                        jQuery('#progress').removeClass('active progress-striped');
                        info('Done with some errors!');
                    }
                })
                .always(function(data) {
                    index += 1;
                    loop();
                });
            }
            loop();
    });
});
</script>