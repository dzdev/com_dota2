<?php
/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');
JHtml::_('jquery.ui');
// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_dota2/assets/css/dota2.css');

if (!isset($this->item->params['rating1'])) {
    $this->item->params['rating1'] = 0;
    $this->item->params['rating2'] = 0;
    $this->item->params['rating3'] = 0;
    $this->item->params['rating4'] = 0;
}
$rating_fieldset = $this->form->getFieldset('rating');
$alt_language_fieldset = $this->form->getFieldset('alt_language');
$additional_fieldset = $this->form->getFieldset('additional');
?>
<script type="text/javascript">
Joomla.submitbutton = function(task)
{
    if(task == 'hero.cancel'){
        Joomla.submitform(task, document.getElementById('hero-form'));
    } else {
        if (task != 'hero.cancel' && document.formvalidator.isValid(document.id('hero-form'))) {
            Joomla.submitform(task, document.getElementById('hero-form'));
        } else {
            alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
        }
    }
}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_dota2&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="hero-form" class="form-validate">
    <div class="form-inline form-inline-header">
        <div class="control-group">
            <div class="control-label"><?php echo $this->form->getLabel('dname'); ?></div>
            <div class="controls"><?php echo $this->form->getInput('dname'); ?></div>
        </div>
        <div class="control-group">
            <div class="control-label"><?php echo $this->form->getLabel('name'); ?></div>
            <div class="controls"><?php echo $this->form->getInput('name'); ?></div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12 form-horizontal">
            <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general') ); ?>

            <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_DOTA2_HERO_DETAILS', true)); ?>
            <div class="row-fluid">
                <div class="span4">
                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('title'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('title'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('tags'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('tags'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('faction'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('faction'); ?></div>
                    </div>
                </div>
                <div class="span4">
                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('atk'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('atk'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('droles'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('droles'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('pa'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('pa'); ?></div>
                    </div>
                </div>
                <div class="span4">
                    <div class="control-group">
                        <div class="control-label"><?php echo $additional_fieldset['jform_params_cover']->label; ?></div>
                        <div class="controls"><?php echo $additional_fieldset['jform_params_cover']->input; ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label"><?php echo $additional_fieldset['jform_params_artwork_dir']->label; ?></div>
                        <div class="controls"><?php echo $additional_fieldset['jform_params_artwork_dir']->input; ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label"><?php echo $additional_fieldset['jform_params_media']->label; ?></div>
                        <div class="controls"><?php echo $additional_fieldset['jform_params_media']->input; ?></div>
                    </div>
                    
                </div>
            </div>

            <div class="row-fluid">
                <div class="span8">
                  <h1>Mô tả ngắn</h1>
                    <?php echo $alt_language_fieldset['jform_params_bio_alt']->input; ?>
                    <h1>Lore</h1>
                    <div class="well">
                    <?php echo $this->item->bio; ?>
                    </div>
          <h1>Mini Guide</h1>

                    <?php echo $additional_fieldset['jform_params_guide']->input; ?>

                </div>
                <div class="span4">
                <h1>Data</h1>
                <div class="control-group">
                  <div class="control-label">Carry</div>
                    <div class="controls"><?php echo $rating_fieldset['jform_params_rating1']->input; ?></div>
        </div>
                <div class="control-group">
                  <div class="control-label">Support</div>
                    <div class="controls"><?php echo $rating_fieldset['jform_params_rating2']->input; ?></div>
        </div>
                <div class="control-group">
                  <div class="control-label">Tank</div>
                    <div class="controls"><?php echo $rating_fieldset['jform_params_rating3']->input; ?></div>
        </div>
                <div class="control-group">
                  <div class="control-label">Hard</div>
                    <div class="controls"><?php echo $rating_fieldset['jform_params_rating4']->input; ?></div>
        </div>
                <div class="control-group">
                  <div class="control-label">Strength</div>
                    <div class="controls">
                    <input type="text" name="jform[attribs][str][b]" class="span2 validate-numeric" value="<?php echo $this->item->attribs['str']['b']; ?>" />
                    + <input type="text" name="jform[attribs][str][g]" class="span2 validate-numeric" value="<?php echo $this->item->attribs['str']['g']; ?>" />
                    </div>
        </div>
                <div class="control-group">
                  <div class="control-label">Agility</div>
                    <div class="controls">
                    <input type="text" name="jform[attribs][agi][b]" class="span2 validate-numeric" value="<?php echo $this->item->attribs['agi']['b']; ?>" />
                    + <input type="text" name="jform[attribs][agi][g]" class="span2 validate-numeric" value="<?php echo $this->item->attribs['agi']['g']; ?>" />
                    </div>
        </div>
                <div class="control-group">
                  <div class="control-label">Intelligence</div>
                    <div class="controls">
                    <input type="text" name="jform[attribs][int][b]" class="span2 validate-numeric" value="<?php echo $this->item->attribs['int']['b']; ?>" />
                    + <input type="text" name="jform[attribs][int][g]" class="span2 validate-numeric" value="<?php echo $this->item->attribs['int']['g']; ?>" />
                    </div>
        </div>
                <div class="control-group">
                  <div class="control-label">Damage</div>
                    <div class="controls">
                      <input type="text" name="jform[attribs][dmg][min]" class="span2" value="<?php echo $this->item->attribs['dmg']['min']; ?>" /> -> <input type="text" name="jform[attribs][dmg][max]" class="span2" value="<?php echo $this->item->attribs['dmg']['max']; ?>" />
                    </div>
        </div>
                <div class="control-group">
                  <div class="control-label"><?php echo JText::_('COM_DOTA2_PRIM_ATTRIB_MS'); ?></div>
                    <div class="controls">
                      <input type="text" name="jform[attribs][ms]" class="span4" value="<?php echo $this->item->attribs['ms']; ?>" />
                    </div>
        </div>
                <div class="control-group">
                  <div class="control-label"><?php echo JText::_('COM_DOTA2_PRIM_ATTRIB_ARMOR'); ?></div>
                    <div class="controls">
                      <input type="text" name="jform[attribs][armor]" class="span4" value="<?php echo $this->item->attribs['armor']; ?>" />
                    </div>
        </div>
                <div class="control-group">
                  <div class="control-label">Range</div>
                    <div class="controls">
                      <input type="text" name="jform[attribs][range]" class="span4" value="<?php echo $this->item->attribs['range']; ?>" />
                    </div>
        </div>
                <div class="control-group">
                  <div class="control-label">BAT</div>
                    <div class="controls">
                      <input type="text" name="jform[attribs][base_attack_time]" class="span4" value="<?php echo $this->item->attribs['base_attack_time']; ?>" />
                    </div>
        </div>
                <div class="control-group">
                  <div class="control-label">Day Vision</div>
                    <div class="controls">
                      <input type="text" name="jform[attribs][day_vision]" class="span4" value="<?php echo $this->item->attribs['day_vision']; ?>" />
                    </div>
        </div>
                <div class="control-group">
                  <div class="control-label">Night Vision</div>
                    <div class="controls">
                      <input type="text" name="jform[attribs][night_vision]" class="span4" value="<?php echo $this->item->attribs['night_vision']; ?>" />
                    </div>
        </div>






                </div>
            </div>
            <?php echo JHtml::_('bootstrap.endTab'); ?>

            <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'publishing', JText::_('COM_DOTA2_FIELDSET_PUBLISHING')); ?>
            <fieldset class="adminform">
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('id'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('id'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('state'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('state'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('created_by'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('created_by'); ?></div>
                </div>
            </fieldset>
            <?php echo JHtml::_('bootstrap.endTab'); ?>

            <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'metadata', JText::_('COM_DOTA2_FIELDSET_METADATA')); ?>
            <fieldset class="adminform">
                <?php echo JLayoutHelper::render('joomla.edit.metadata', $this); ?>
            </fieldset>
            <?php echo JHtml::_('bootstrap.endTab'); ?>

            <?php if (JFactory::getUser()->authorise('core.admin','dota2')): ?>
            <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'permissions', JText::_('COM_DOTA2_FIELDSET_RULES')); ?>
            <fieldset class="adminform">
                <?php echo JHtml::_('sliders.start', 'permissions-sliders-'.$this->item->id, array('useCookie'=>1)); ?>
                <?php echo JHtml::_('sliders.panel', JText::_('ACL Configuration'), 'access-rules'); ?>
                <?php echo $this->form->getInput('rules'); ?>
                <?php echo JHtml::_('sliders.end'); ?>
            </fieldset>
            <?php echo JHtml::_('bootstrap.endTab'); ?>
            <?php endif; ?>
            <?php echo JHtml::_('bootstrap.endTabSet'); ?>
        </div>

        <div class="clr"></div>

        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>

    </div>
</form>