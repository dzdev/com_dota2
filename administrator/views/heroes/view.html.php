<?php
/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');

/**
 * View class for a list of Dota2.
 */
class Dota2ViewHeroes extends JViewLegacy
{
    protected $items;
    protected $pagination;
    protected $state;

    /**
     * Display the view
     */
    public function display($tpl = null)
    {
        $this->state        = $this->get('State');
        $this->items        = $this->get('Items');
        $this->pagination   = $this->get('Pagination');

        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
            throw new Exception(implode("\n", $errors));
        }

        Dota2Helper::addSubmenu('heroes');

        $this->addToolbar();

        $this->sidebar = JHtmlSidebar::render();
        parent::display($tpl);
    }

    /**
     * Add the page title and toolbar.
     *
     * @since   1.6
     */
    protected function addToolbar()
    {
        require_once JPATH_COMPONENT.'/helpers/dota2.php';

        $state  = $this->get('State');
        $canDo  = Dota2Helper::getActions($state->get('filter.category_id'));

        JToolBarHelper::title(JText::_('COM_DOTA2_TITLE_HEROES'), 'heroes.png');

        //Check if the form exists before showing the add/edit buttons
        $formPath = JPATH_COMPONENT_ADMINISTRATOR.'/views/hero';
        if (file_exists($formPath)) {

            if ($canDo->get('core.create')) {
                // Add popup button
                JToolbar::getInstance('toolbar')->appendButton('Popup', 'box-add', 'COM_DOTA2_POPULATE_DATA', 'index.php?option=com_dota2&tmpl=component&view=getheroes', 600, 400);
                JToolbar::getInstance('toolbar')->appendButton('Popup', 'download', 'COM_DOTA2_QUICK_POPULATE_DATA', 'index.php?option=com_dota2&tmpl=component&view=getheroes&quick_mode=1', 600, 400);
                JToolBarHelper::addNew('hero.add','JTOOLBAR_NEW');
            }

            if ($canDo->get('core.edit') && isset($this->items[0])) {
                JToolBarHelper::editList('hero.edit','JTOOLBAR_EDIT');
            }

        }

        if ($canDo->get('core.edit.state')) {

            if (isset($this->items[0]->state)) {
                JToolBarHelper::divider();
                JToolBarHelper::custom('heroes.publish', 'publish.png', 'publish_f2.png','JTOOLBAR_PUBLISH', true);
                JToolBarHelper::custom('heroes.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
            } else if (isset($this->items[0])) {
                //If this component does not use state then show a direct delete button as we can not trash
                JToolBarHelper::deleteList('', 'heroes.delete','JTOOLBAR_DELETE');
            }

            if (isset($this->items[0]->state)) {
                JToolBarHelper::divider();
                JToolBarHelper::archiveList('heroes.archive','JTOOLBAR_ARCHIVE');
            }
            if (isset($this->items[0]->checked_out)) {
                JToolBarHelper::custom('heroes.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
            }
        }

        //Show trash and delete for components that uses the state field
        if (isset($this->items[0]->state)) {
            if ($state->get('filter.state') == -2 && $canDo->get('core.delete')) {
                JToolBarHelper::deleteList('', 'heroes.delete','JTOOLBAR_EMPTY_TRASH');
                JToolBarHelper::divider();
            } else if ($canDo->get('core.edit.state')) {
                JToolBarHelper::trash('heroes.trash','JTOOLBAR_TRASH');
                JToolBarHelper::divider();
            }
        }

        if ($canDo->get('core.admin')) {
            JToolBarHelper::preferences('com_dota2');
        }

        //Set sidebar action - New in 3.0
        JHtmlSidebar::setAction('index.php?option=com_dota2&view=heroes');

        $this->extra_sidebar = '';

        JHtmlSidebar::addFilter(

            JText::_('JOPTION_SELECT_PUBLISHED'),

            'filter_published',

            JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), "value", "text", $this->state->get('filter.state'), true)

        );

        //Filter for the field atk
        $select_label = JText::sprintf('COM_DOTA2_FILTER_SELECT_LABEL', 'Faction');
        JHtmlSidebar::addFilter(
            $select_label,
            'filter_faction',
            JHtml::_('select.options', JHtml::_('dota2.faction') , "value", "text", $this->state->get('filter.faction'), true)
        );

        //Filter for the field atk
        $select_label = JText::sprintf('COM_DOTA2_FILTER_SELECT_LABEL', 'Attack');
        JHtmlSidebar::addFilter(
            $select_label,
            'filter_atk',
            JHtml::_('select.options', JHtml::_('dota2.atks') , "value", "text", $this->state->get('filter.atk'), true)
        );

        //Filter for the field pa
        $select_label = JText::sprintf('COM_DOTA2_FILTER_SELECT_LABEL', 'Primary Attribute');
        JHtmlSidebar::addFilter(
            $select_label,
            'filter_pa',
            JHtml::_('select.options', JHtml::_('dota2.pas') , "value", "text", $this->state->get('filter.pa'), true)
        );

        //Filter for field roles
        $select_label = JText::sprintf('COM_DOTA2_FILTER_SELECT_LABEL', 'Roles');
        JHtmlSidebar::addFilter(
            $select_label,
            'filter_roles',
            JHtml::_('select.options', JHtml::_('dota2.roles') , "value", "text", $this->state->get('filter.roles'), true)
        );
    }

    protected function getSortFields()
    {
        return array(
        'a.id' => JText::_('JGRID_HEADING_ID'),
        'a.ordering' => JText::_('JGRID_HEADING_ORDERING'),
        'a.state' => JText::_('JSTATUS'),
        'a.checked_out' => JText::_('COM_DOTA2_HEROES_CHECKED_OUT'),
        'a.checked_out_time' => JText::_('COM_DOTA2_HEROES_CHECKED_OUT_TIME'),
        'a.created_by' => JText::_('COM_DOTA2_HEROES_CREATED_BY'),
        'a.dname' => JText::_('COM_DOTA2_HEROES_DNAME'),
        'a.droles' => JText::_('COM_DOTA2_HEROES_DROLES'),
        'a.pa' => JText::_('COM_DOTA2_HEROES_PA'),
        );
    }


}
