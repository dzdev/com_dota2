<?php
/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_dota2/assets/css/dota2.css');
?>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function(){

    js('input:hidden.components').each(function(){
        var name = js(this).attr('name');
        if(name.indexOf('componentshidden')){
            js('#jform_components option[value="'+js(this).val()+'"]').attr('selected',true);
        }
    });
    js("#jform_components").trigger("liszt:updated");
    });

    Joomla.submitbutton = function(task)
    {
        if(task == 'item.cancel'){
            Joomla.submitform(task, document.getElementById('item-form'));
        }
        else{

            if (task != 'item.cancel' && document.formvalidator.isValid(document.id('item-form'))) {

    if(js('#jform_components option:selected').length == 0){
        js("#jform_components option[value=0]").attr('selected','selected');
    }
                 // Disable all empty select before submit to avoid abundant values
          jQuery(".item-component-wrapper select").filter(function() {return jQuery(this).val() == ""}).attr('disabled', 'disabled');
                Joomla.submitform(task, document.getElementById('item-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>

<form action="<?php echo JRoute::_('index.php?option=com_dota2&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="item-form" class="form-validate">
    <div class="form-inline form-inline-header">
        <div class="control-group">
            <div class="control-label"><?php echo $this->form->getLabel('dname'); ?></div>
            <div class="controls"><?php echo $this->form->getInput('dname'); ?></div>
        </div>
        <div class="control-group">
            <div class="control-label"><?php echo $this->form->getLabel('name'); ?></div>
            <div class="controls"><?php echo $this->form->getInput('name'); ?></div>
        </div>
    </div>
    <div class="form-horizontal">
        <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general') ); ?>

        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_DOTA2_ITEM_DETAILS', true)); ?>
        <div class="row-fluid">
            <div class="span4">
                
                
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('qual'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('qual'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('category', 'params'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('category', 'params'); ?></div>
                </div>
                 <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('created'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('created'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('tags'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('tags'); ?></div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('cd'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('cd'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('cost'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('cost'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('recipe_price', 'params'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('recipe_price', 'params'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('mc'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('mc'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('dota_id'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('dota_id'); ?></div>
                </div>
            </div>
            <div class="span4">
                <?php echo $this->form->getInput('components'); ?>
                <?php
                    foreach((array)$this->item->components as $value):
                        if(!is_array($value)):
                            echo '<input type="hidden" class="components" name="jform[componentshidden]['.$value.']" value="'.$value.'" />';
                        endif;
                    endforeach;
                ?>
            </div>
        </div>

        <div class="row-fluid">
          <div class="span9">
              <h1>Mô tả item</h1>
              <?php
                    $alt_language_fieldset = $this->form->getFieldset('alt_language');
                    echo $alt_language_fieldset['jform_params_attrib_alt']->input;
                ?>
                <h1>Tip/Trick</h1>
              <?php
                    $alt_language_fieldset = $this->form->getFieldset('alt_language');
                    echo $alt_language_fieldset['jform_params_desc_alt']->input;
                ?>
            </div>
            <div class="span3">
                <h1>English</h1>
                <div class="well"><?php echo $this->item->lore; ?></div>
                <div class="well">
        <?php echo $this->item->attrib; ?>
                    <br  />
                    <?php echo $this->item->desc; ?>
                </div>
            </div>

        </div>





        <?php echo JHtml::_('bootstrap.endTab'); ?>

        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'publishing', JText::_('COM_DOTA2_FIELDSET_PUBLISHING')); ?>
        <fieldset class="adminform">
            <div class="control-group">
                <div class="control-label"><?php echo $this->form->getLabel('id'); ?></div>
                <div class="controls"><?php echo $this->form->getInput('id'); ?></div>
            </div>
            <div class="control-group">
                <div class="control-label"><?php echo $this->form->getLabel('state'); ?></div>
                <div class="controls"><?php echo $this->form->getInput('state'); ?></div>
            </div>
            <div class="control-group">
                <div class="control-label"><?php echo $this->form->getLabel('created_by'); ?></div>
                <div class="controls"><?php echo $this->form->getInput('created_by'); ?></div>
            </div>
        </fieldset>
        <?php echo JHtml::_('bootstrap.endTab'); ?>

        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'metadata', JText::_('COM_DOTA2_FIELDSET_METADATA')); ?>
            <?php echo JLayoutHelper::render('joomla.edit.metadata', $this); ?>
        <?php echo JHtml::_('bootstrap.endTab'); ?>

        <?php if (JFactory::getUser()->authorise('core.admin','dota2')): ?>
        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'permissions', JText::_('COM_DOTA2_FIELDSET_RULES')); ?>
        <fieldset class="panelform">
            <?php echo JHtml::_('sliders.start', 'permissions-sliders-'.$this->item->id, array('useCookie'=>1)); ?>
            <?php echo JHtml::_('sliders.panel', JText::_('ACL Configuration'), 'access-rules'); ?>
            <?php echo $this->form->getInput('rules'); ?>
            <?php echo JHtml::_('sliders.end'); ?>
        </fieldset>
        <?php echo JHtml::_('bootstrap.endTab'); ?>
        <?php endif; ?>
        <?php echo JHtml::_('bootstrap.endTabSet'); ?>
        
        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>
    </div>
        
</form>