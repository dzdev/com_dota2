<?php
/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */

// no direct access
defined('_JEXEC') or die;

$app = JFactory::getApplication();

if ($app->isSite())
{
    JSession::checkToken('get') or die(JText::_('JINVALID_TOKEN'));
}

JHtml::_('bootstrap.tooltip');
JHtml::_('formbehavior.chosen', 'select');


$user   = JFactory::getUser();
$userId = $user->get('id');
$listOrder  = $this->state->get('list.ordering');
$listDirn   = $this->state->get('list.direction');
$canOrder   = $user->authorise('core.edit.state', 'com_dota2');
$saveOrder  = $listOrder == 'a.ordering';
if ($saveOrder)
{
    $saveOrderingUrl = 'index.php?option=com_dota2&task=items.saveOrderAjax&tmpl=component';
    JHtml::_('sortablelist.sortable', 'itemList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
$function  = $app->input->getCmd('function', 'dota2SelectItem');
$form      = $app->input->getCmd('form', '');
$sortFields = $this->getSortFields();
?>
<script type="text/javascript">
    Joomla.orderTable = function() {
        table = document.getElementById("sortTable");
        direction = document.getElementById("directionTable");
        order = table.options[table.selectedIndex].value;
        if (order != '<?php echo $listOrder; ?>') {
            dirn = 'asc';
        } else {
            dirn = direction.options[direction.selectedIndex].value;
        }
        Joomla.tableOrdering(order, dirn, '');
    }
</script>
<form action="<?php echo JRoute::_('index.php?option=com_dota2&view=items&layout=modal&tmpl=component&function='.$function.'&'.JSession::getFormToken().'=1&form='.$form); ?>" method="post" name="adminForm" id="adminForm">
    <fieldset class="filter clearfix">
        <div class="btn-toolbar">
            <div class="btn-group pull-left">
                <label for="filter_search">
                    <?php echo JText::_('JSEARCH_FILTER_LABEL'); ?>
                </label>
            </div>
            <div class="btn-group pull-left">
                <input type="text" name="filter_search" id="filter_search" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" size="30" title="<?php echo JText::_('COM_CONTENT_FILTER_SEARCH_DESC'); ?>" />
            </div>
            <div class="btn-group pull-left">
                <button type="submit" class="btn hasTooltip" data-placement="bottom" title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>">
                    <span class="icon-search"></span><?php echo '&#160;' . JText::_('JSEARCH_FILTER_SUBMIT'); ?></button>
                <button type="button" class="btn hasTooltip" data-placement="bottom" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>" onclick="document.getElementById('filter_search').value='';this.form.submit();">
                    <span class="icon-remove"></span><?php echo '&#160;' . JText::_('JSEARCH_FILTER_CLEAR'); ?></button>
            </div>
            <div class="clearfix"></div>
        </div>
        <hr class="hr-condensed" />
        <div class="filters pull-left">
            <select name="filter_components" class="input-medium" onchange="this.form.submit()">
                <option value=""><?php echo JText::sprintf('COM_DOTA2_FILTER_SELECT_LABEL', 'Component');?></option>
                <?php echo JHtml::_('select.options', JHtml::_('dota2.components'), 'value', 'text', $this->state->get('filter.components'));?>
            </select>
            <select name="filter_created" class="input-medium" onchange="this.form.submit()">
                <option value=""><?php echo JText::sprintf('COM_DOTA2_FILTER_SELECT_LABEL', 'Created by components');?></option>
                <?php echo JHtml::_('select.options', JHtml::_('dota2.boolean'), 'value', 'text', $this->state->get('filter.created'));?>
            </select>
            <select name="filter_qual" class="input-medium" onchange="this.form.submit()">
                <option value=""><?php echo JText::sprintf('COM_DOTA2_FILTER_SELECT_LABEL', 'Quality');?></option>
                <?php echo JHtml::_('select.options', JHtml::_('dota2.qualities'), 'value', 'text', $this->state->get('filter.quals'));?>
            </select>
        </div>
    </fieldset>
    <br />
    <div class="clearfix"> </div>
    <div id="notice" style="overflow: hidden"></div>
    <div class="clearfix"></div>
    <table class="table table-striped" id="itemList">
        <thead>
            <tr>
            <th class='center' width="10%"><?php echo JText::_('COM_DOTA2_IMAGE'); ?></th>
            <th class='left'>
            <?php echo JHtml::_('grid.sort',  'COM_DOTA2_ITEMS_DNAME', 'a.dname', $listDirn, $listOrder); ?>
            </th>
            <th width="10%" class='center'>
            <?php echo JHtml::_('grid.sort',  'COM_DOTA2_ITEMS_QUAL', 'a.qual', $listDirn, $listOrder); ?>
            </th>
            <th width="80">
            </th>
            </tr>
        </thead>
        <tfoot>
            <?php
            if(isset($this->items[0])){
                $colspan = count(get_object_vars($this->items[0]));
            }
            else{
                $colspan = 10;
            }
        ?>
        <tr>
            <td colspan="<?php echo $colspan ?>">
                <?php echo $this->pagination->getListFooter(); ?>
            </td>
        </tr>
        </tfoot>
        <tbody>
        <?php foreach ($this->items as $i => $item) :
            $ordering   = ($listOrder == 'a.ordering');
            $canCreate  = $user->authorise('core.create',       'com_dota2');
            $canEdit    = $user->authorise('core.edit',         'com_dota2');
            $canCheckin = $user->authorise('core.manage',       'com_dota2');
            $canChange  = $user->authorise('core.edit.state',   'com_dota2');
            ?>
            <tr class="row<?php echo $i % 2; ?>">
            <td class="center">
                <img src="<?php echo JUri::root().'media/com_dota2/images/dota2/items/'.$item->name; ?>_lg.png" style="max-height: 40px;" />
            </td>
            <td>
                <a href="<?php echo JRoute::_('index.php?option=com_dota2&task=item.edit&id=' . $item->id); ?>" title="<?php echo JText::_('JACTION_EDIT'); ?>" target="_blank">
                    <?php echo $this->escape($item->dname); ?> <i class="icon-out"></i>
                </a>
            </td>
            <td class="center">
                <?php echo JText::_('COM_DOTA2_ITEM_QUALITY_'.$item->qual); ?>
            </td>
            <td>
                <a class="btn btn-sm btn-success dota2-item" data-name="<?php echo $this->escape(addslashes($item->name)) ?>">
                    <i class="icon-plus"></i> <?php echo JText::_('COM_DOTA2_INSERT'); ?>
                </a>
            </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <input type="hidden" name="task" value="" />
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
    <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
    <?php echo JHtml::_('form.token'); ?>
</form>
<style type="text/css">.pointer { cursor: pointer; }</style>
<script type="text/javascript">
jQuery('a.dota2-item').on('click', function() {
    if (window.parent) {
        jQuery('.alert').off('closed').alert('close');
        window.parent.<?php echo $this->escape($function); ?>(jQuery(this).attr('data-name'), '<?php echo $form; ?>');
        var $alert = jQuery('<div class="alert alert-success fade" ><a class="close" data-dismiss="alert" href="#">&times;</a>Added <b>{loaditem ' + jQuery(this).attr('data-name') + '}</b></div>').alert();
        jQuery('#notice').append($alert).innerHeight($alert.outerHeight()); $alert.addClass('in');
        $alert.on('closed', function() {jQuery('#notice').height(0)});
    }
});
</script>