<?php
/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
/**
 * View class for a list of Dota2.
 */
class Dota2ViewItems extends JViewLegacy
{
    protected $items;
    protected $pagination;
    protected $state;

    /**
     * Display the view
     */
    public function display($tpl = null)
    {
        $this->state        = $this->get('State');
        $this->items        = $this->get('Items');
        $this->pagination   = $this->get('Pagination');

        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
            throw new Exception(implode("\n", $errors));
        }
        
        Dota2Helper::addSubmenu('items');
        
        $this->addToolbar();
        
        $this->sidebar = JHtmlSidebar::render();
        parent::display($tpl);
    }

    /**
     * Add the page title and toolbar.
     *
     * @since   1.6
     */
    protected function addToolbar()
    {
        require_once JPATH_COMPONENT.'/helpers/dota2.php';

        $state  = $this->get('State');
        $canDo  = Dota2Helper::getActions($state->get('filter.category_id'));

        JToolBarHelper::title(JText::_('COM_DOTA2_TITLE_ITEMS'), 'items.png');
        
        //Check if the form exists before showing the add/edit buttons
        $formPath = JPATH_COMPONENT_ADMINISTRATOR.'/views/item';
        if (file_exists($formPath)) {

            if ($canDo->get('core.create')) {
                JToolbar::getInstance('toolbar')->appendButton('Popup', 'box-add', 'COM_DOTA2_POPULATE_DATA', 'index.php?option=com_dota2&tmpl=component&view=getitems', 600, 400);
                JToolbar::getInstance('toolbar')->appendButton('Popup', 'download', 'COM_DOTA2_POPULATE_DATA', 'index.php?option=com_dota2&tmpl=component&view=getitems&quick_mode=1', 600, 400);
                JToolBarHelper::addNew('item.add','JTOOLBAR_NEW');
            }

            if ($canDo->get('core.edit') && isset($this->items[0])) {
                JToolBarHelper::editList('item.edit','JTOOLBAR_EDIT');
            }

        }

        if ($canDo->get('core.edit.state')) {

            if (isset($this->items[0]->state)) {
                JToolBarHelper::divider();
                JToolBarHelper::custom('items.publish', 'publish.png', 'publish_f2.png','JTOOLBAR_PUBLISH', true);
                JToolBarHelper::custom('items.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
            } else if (isset($this->items[0])) {
                //If this component does not use state then show a direct delete button as we can not trash
                JToolBarHelper::deleteList('', 'items.delete','JTOOLBAR_DELETE');
            }

            if (isset($this->items[0]->state)) {
                JToolBarHelper::divider();
                JToolBarHelper::archiveList('items.archive','JTOOLBAR_ARCHIVE');
            }
            if (isset($this->items[0]->checked_out)) {
                JToolBarHelper::custom('items.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
            }
        }
        
        //Show trash and delete for components that uses the state field
        if (isset($this->items[0]->state)) {
            if ($state->get('filter.state') == -2 && $canDo->get('core.delete')) {
                JToolBarHelper::deleteList('', 'items.delete','JTOOLBAR_EMPTY_TRASH');
                JToolBarHelper::divider();
            } else if ($canDo->get('core.edit.state')) {
                JToolBarHelper::trash('items.trash','JTOOLBAR_TRASH');
                JToolBarHelper::divider();
            }
        }

        if ($canDo->get('core.admin')) {
            JToolBarHelper::preferences('com_dota2');
        }
        
        //Set sidebar action - New in 3.0
        JHtmlSidebar::setAction('index.php?option=com_dota2&view=items');
        
        $this->extra_sidebar = '';
        
        JHtmlSidebar::addFilter(

            JText::_('JOPTION_SELECT_PUBLISHED'),

            'filter_published',

            JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), "value", "text", $this->state->get('filter.state'), true)

        );

        JHtmlSidebar::addFilter(
            'Components',
            'filter_components',
            JHtml::_('select.options', JHtml::_('dota2.components'), "value", "text", $this->state->get('filter.components')),
            true
        );
        //Filter for the field created
        $select_label = JText::sprintf('COM_DOTA2_FILTER_SELECT_LABEL', 'Created by components');
        JHtmlSidebar::addFilter(
            $select_label,
            'filter_created',
            JHtml::_('select.options', JHtml::_('dota2.boolean') , "value", "text", $this->state->get('filter.created'), true)
        );

        //Filter for the field qual
        $select_label = JText::sprintf('COM_DOTA2_FILTER_SELECT_LABEL', 'Quality');

        JHtmlSidebar::addFilter(
            $select_label,
            'filter_qual',
            JHtml::_('select.options', JHtml::_('dota2.qualities') , "value", "text", $this->state->get('filter.qual'), true)
        );

        
    }
    
    protected function getSortFields()
    {
        return array(
        'a.id' => JText::_('JGRID_HEADING_ID'),
        'a.ordering' => JText::_('JGRID_HEADING_ORDERING'),
        'a.state' => JText::_('JSTATUS'),
        'a.checked_out' => JText::_('COM_DOTA2_ITEMS_CHECKED_OUT'),
        'a.checked_out_time' => JText::_('COM_DOTA2_ITEMS_CHECKED_OUT_TIME'),
        'a.created_by' => JText::_('COM_DOTA2_ITEMS_CREATED_BY'),
        'a.dname' => JText::_('COM_DOTA2_ITEMS_DNAME'),
        'a.cost' => JText::_('COM_DOTA2_ITEMS_COST'),
        'a.created' => JText::_('COM_DOTA2_ITEMS_CREATED'),
        'a.desc' => JText::_('COM_DOTA2_ITEMS_DESC'),
        'a.dota_id' => JText::_('COM_DOTA2_ITEMS_DOTA_ID'),
        'a.lore' => JText::_('COM_DOTA2_ITEMS_LORE'),
        'a.mc' => JText::_('COM_DOTA2_ITEMS_MC'),
        'a.qual' => JText::_('COM_DOTA2_ITEMS_QUAL'),
        );
    }

    
}
