jQuery(window).load(function() {
    /* DECLARATION */
    var $selectpicker = jQuery('.selectpicker'),
//         $selectPa = jQuery('select.filter-pa'),
        $selectRole = jQuery('select.filter-role'),
        $selectAtk = jQuery('select.filter-atk'),
        $selectName = jQuery('select.filter-name'),
        $selectNameOptions = jQuery('select.filter-name>option'),
        $sortOrder = jQuery('select.sort-order'),
        $container = jQuery('.isotope');

        /* INITIALIZATION */

        $container.isotope({
            animationOptions:{duration: 400},
            getSortData: {
                rating1: function ($elem) {
                    return $elem.attr('data-sort-rating1');
                },
                rating2: function ($elem) {
                    return $elem.attr('data-sort-rating2');
                },
                rating3: function ($elem) {
                    return $elem.attr('data-sort-rating3');
                },
                rating4: function ($elem) {
                    return $elem.attr('data-sort-rating4');
                },
                ordering: function ($elem) {
                    return parseInt($elem.attr('data-sort-ordering'));
                }
            }
        });

        $selectpicker.selectpicker();

        /* EVENTS BINDING */

        $selectpicker.on('change', function() {
            jQuery.cookie(jQuery(this).attr('data-name'), jQuery(this).val(), { path: '/' });
            var filters = '', enabled;

            // Get filter classes
//             if ($selectPa.val().length != 0)
//                 filters = filters + '.' + $selectPa.val();
            if ($selectRole.val().length != 0)
                filters = filters + '.' + $selectRole.val();
            if ($selectAtk.val().length != 0)
                filters = filters + '.' + $selectAtk.val();
            if ($selectName.val().length != 0)
                filters = filters + '.' + $selectName.val();
            if (filters.length == 0)
                filters = '*';

            // Filter the heroes' name option
            // + Do not use its internal options to filter its self
            // + Refilter when change the option back to All Heroes
            if (jQuery(this)[0] != $selectName[0] || ( jQuery(this)[0] == $selectName[0] && $selectName.val().length == 0 )) {
                $selectNameOptions.filter(':disabled').removeAttr('disabled');
                enabled = $selectNameOptions.filter(filters + ', .name-all');
                $selectNameOptions.not(enabled).attr('disabled', 'disabled');
                $selectName.selectpicker('render');
            }

            if ($selectName.val() == null)
                $selectName.selectpicker('val', "");
            else
                $container.isotope({filter: filters})
        });


        $sortOrder.selectpicker().on('change', function() {
            jQuery.cookie(jQuery(this).attr('data-name'), jQuery(this).val(), { path: '/' });
            order = jQuery(this).val();
            if (order == 'original-order' || order == 'ordering')
                $container.isotope({sortBy: jQuery(this).val(), sortAscending: true});
            else
                $container.isotope({sortBy: jQuery(this).val(), sortAscending: false});
        });

        /* SET FILTER AND SORT STATES FROM COOKIES*/
        $selectpicker.each(function(){
            jQuery(this).selectpicker('val', jQuery.cookie(jQuery(this).attr('data-name')));
        });
        $sortOrder.selectpicker('val', jQuery.cookie('sort-order'));
});