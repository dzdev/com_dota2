<?php
/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */

// No direct access.
defined('_JEXEC') or die;

require_once JPATH_COMPONENT.'/controller.php';

/**
 * Abilities list controller class.
 */
class Dota2ControllerAbilities extends Dota2Controller
{
    /**
     * Proxy for getModel.
     * @since   1.6
     */
    public function &getModel($name = 'Abilities', $prefix = 'Dota2Model')
    {
        $model = parent::getModel($name, $prefix, array('ignore_request' => true));
        return $model;
    }
}