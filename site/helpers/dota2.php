<?php
/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */

defined('_JEXEC') or die;

abstract class Dota2Helper
{
    public static function selectAttributes()
    {
        $options = array();
        $options[0] = new stdClass();
        $options[0]->value = "";
        $options[0]->text = "All Attributes";
        $options[3] = new stdClass();
        $options[3]->value = "int";
        $options[3]->text = "Intelligence";
        $options[1] = new stdClass();
        $options[1]->value = "agi";
        $options[1]->text = "Agility";
        $options[2] = new stdClass();
        $options[2]->value = "str";
        $options[2]->text = "Strength";
        return JHtml::_('select.options', $options , "value", "text");
    }
}

