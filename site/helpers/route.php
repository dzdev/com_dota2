<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Content Component Route Helper
 *
 * @static
 * @package     Joomla.Site
 * @subpackage  com_content
 * @since       1.5
 */
abstract class Dota2HelperRoute
{
    protected static $lookup = null;

    /**
     * @param   integer  The route of the content item
     */
    public static function getHeroRoute($id, $catid = 0, $language = 0)
    {
//         xdebug_break();
        //Create the link
        $link = 'index.php?option=com_dota2&view=hero&id='. (int) $id;

        if ($itemId = self::_findItemid('heroes'))
            $link .= '&Itemid='.$itemId;

        return $link;
    }

    public static function getItemRoute($id, $catid = 0, $language = 0)
    {
        //Create the link
        $link = 'index.php?option=com_dota2&view=item&id='. (int) $id;

        if ($itemId = self::_findItemid('items'))
            $link .= '&Itemid='.$itemId;

        return $link;
    }

    public static function getAbilityRoute($id, $catid = 0, $language = 0)
    {
        //Create the link
        $link = 'index.php?option=com_dota2&view=ability&id='. (int) $id;

        if ($itemId = self::_findItemid('abilities'))
            $link .= '&Itemid='.$itemId;

        return $link;
    }


    protected  static function _findItemid($needle)
    {
        $app        = JFactory::getApplication();
        $menus      = $app->getMenu('site');

        // Prepare the reverse lookup array.
        if (self::$lookup === null)
        {
            self::$lookup = array();

            $component  = JComponentHelper::getComponent('com_dota2');
            $items      = $menus->getItems('component_id', $component->id);
            foreach ($items as $item)
            {
                if (isset($item->query) && isset($item->query['view']))
                {
                    $view = $item->query['view'];
                    self::$lookup[$view] = $item->id;
                }
            }
        }

        if (isset(self::$lookup[$needle]))
            return self::$lookup[$needle];

        return null;
    }
}
