<?php

/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

require_once JPATH_SITE . '/components/com_dota2/helpers/route.php';

/**
 * Methods supporting a list of Dota2 records.
 */
class Dota2ModelAbilities extends JModelList {

    /**
     * Constructor.
     *
     * @param    array    An optional associative array of configuration settings.
     * @see        JController
     * @since    1.6
     */
    public function __construct($config = array()) {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                                'id', 'a.id',
                'ordering', 'a.ordering',
                'state', 'a.state',
                'created_by', 'a.created_by',
                'name', 'a.name',
                'dname', 'a.dname',
                'affects', 'a.affects',
                'attrib', 'a.attrib',
                'cmb', 'a.cmb',
                'desc', 'a.desc',
                'dmg', 'a.dmg',
                'hurl', 'a.hurl',
                'lore', 'a.lore',
                'params', 'a.params',
                'metakey', 'a.metakey',
                'metadesc', 'a.metadesc',
                'metadata', 'a.metadata',

            );
        }
        
        parent::__construct($config);
    }

    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @since   1.6
     */
    protected function populateState($ordering = null, $direction = null) {

        // Initialise variables.
        $app = JFactory::getApplication();

        // List state information
        $limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'));
        $this->setState('list.limit', $limit);

        $limitstart = JFactory::getApplication()->input->getInt('limitstart', 0);
        $this->setState('list.start', $limitstart);
        
        // Load the filter state.
        $search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);
        
        $hero = $app->getUserStateFromRequest($this->context . '.filter.hero', 'filter_hero');
        $this->setState('filter.hero', $hero);
        
        if(empty($ordering)) {
            $ordering = 'a.dname';
        }

        if (empty($direction)) {
            $direction = 'ASC';
        }
        // List state information.
        parent::populateState($ordering, $direction);
    }

    /**
     * Build an SQL query to load the list data.
     *
     * @return  JDatabaseQuery
     * @since   1.6
     */
    protected function getListQuery() {
        // Create a new query object.
        $db = $this->getDbo();
        $query = $db->getQuery(true);

        // Select the required fields from the table.
        $query->select(
                $this->getState(
                        'list.select', 'a.*'
                )
        );

        $query->from('`#__dota2_abilities` AS a');

        
    // Join over the users for the checked out user.
    $query->select('uc.name AS editor');
    $query->join('LEFT', '#__users AS uc ON uc.id=a.checked_out');
    
        // Join over the created by field 'created_by'
        $query->select('created_by.name AS created_by');
        $query->join('LEFT', '#__users AS created_by ON created_by.id = a.created_by');
        

        // Filter by search in title
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            if (stripos($search, 'id:') === 0) {
                $query->where('a.id = ' . (int) substr($search, 3));
            } else {
                $search = $db->Quote('%' . $db->escape($search, true) . '%');
                $query->where('( a.name LIKE '.$search.'  OR  a.dname LIKE '.$search.' )');
            }
        }
        
        $hero = $this->getState('filter.hero');
        if ($hero)
            $query->where('( a.name LIKE \'' . $db->escape($hero, true) .'%\' )');
        
        // only show published item
        $query->where('a.state = 1');
        
        // Add the list ordering clause.
        $orderCol = $this->state->get('list.ordering');
        $orderDirn = $this->state->get('list.direction');
        if ($orderCol && $orderDirn) {
            $query->order($db->escape($orderCol . ' ' . $orderDirn));
        }

        return $query;
    }
    
    public function getItems() {
        $items = parent::getItems();
        
        foreach ($items as &$item) {
            $item->params = new JRegistry($item->params);
            $item->image['dota2']['hp1'] = JUri::root().'media/com_dota2/images/dota2/abilities/'.$item->name.'_hp1.png';
            $item->image['dota2']['hp2'] = JUri::root().'media/com_dota2/images/dota2/abilities/'.$item->name.'_hp2.png';
            $item->image['dota1']['hp1'] = JUri::root().'media/com_dota2/images/dota1/abilities/'.$item->name.'_hp1.png';
            $item->link = JRoute::_(Dota2HelperRoute::getItemRoute($item->id));
        }
        
        return $items;
    }
}
