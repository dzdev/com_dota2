<?php
/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelform');
jimport('joomla.event.dispatcher');

require_once JPATH_SITE.'/components/com_dota2/helpers/route.php';

/**
 * Dota2 model.
 */
class Dota2ModelHero extends JModelForm
{
    
    var $_item = null;
    
    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @since   1.6
     */
    protected function populateState()
    {
        $app = JFactory::getApplication('com_dota2');

        // Load state from the request userState on edit or from the passed variable on default
        if (JFactory::getApplication()->input->get('layout') == 'edit') {
            $id = JFactory::getApplication()->getUserState('com_dota2.edit.hero.id');
        } else {
            $id = JFactory::getApplication()->input->get('id');
            JFactory::getApplication()->setUserState('com_dota2.edit.hero.id', $id);
        }
        $this->setState('hero.id', $id);

        // Load the parameters.
        $params = $app->getParams();
        $params_array = $params->toArray();
        if(isset($params_array['item_id'])){
            $this->setState('hero.id', $params_array['item_id']);
        }
        $this->setState('params', $params);

    }
        

    /**
     * Method to get an ojbect.
     *
     * @param   integer The id of the object to get.
     *
     * @return  mixed   Object on success, false on failure.
     */
    public function &getData($id = null, $name = null)
    {
        if ($this->_item === null)
        {
            $this->_item = false;

            if (empty($id)) {
                $id = $this->getState('hero.id');
            }
            
            if (!$id) {
                return NULL;
            }
            
            // Get a level row instance.
            $table = $this->getTable();

            // name override id
            if (!empty($name)) {
                $result = $table->load(array('name' => $name));
            } else {
                $result = $table->load($id);
            }
            
            // Attempt to load the row.
            if ($result)
            {
                // Check published state.
                if ($published = $this->getState('filter.published'))
                {
                    if ($table->state != $published) {
                        return $this->_item;
                    }
                }

                // Convert the JTable to a clean JObject.
                $properties = $table->getProperties(1);
                $this->_item = JArrayHelper::toObject($properties, 'JObject');
                $this->_item->attribs = json_decode($this->_item->attribs);
                $this->_item->params  = new JRegistry($this->_item->params);
                $this->_item->image['dota2']['full']    = JUri::root().'media/com_dota2/images/dota2/heroes/'.$this->_item->name.'_full.png';
                $this->_item->image['dota2']['hphover'] = JUri::root().'media/com_dota2/images/dota2/heroes/'.$this->_item->name.'_hphover.png';
                $this->_item->image['dota2']['sb']      = JUri::root().'media/com_dota2/images/dota2/heroes/'.$this->_item->name.'_sb.png';
                $this->_item->image['dota2']['vert']    = JUri::root().'media/com_dota2/images/dota2/heroes/'.$this->_item->name.'_vert.jpg';
                $this->_item->image['dota1']['full']    = JUri::root().'media/com_dota2/images/dota1/heroes/'.$this->_item->name.'_full.png';
                $this->_item->image['dota1']['hphover'] = JUri::root().'media/com_dota2/images/dota1/heroes/'.$this->_item->name.'_hphover.png';
                $this->_item->link = JRoute::_(Dota2HelperRoute::getHeroRoute($this->_item->id));
                $registry = new JRegistry;
                $registry->loadString($this->_item->metadata);
                $this->_item->metadata = $registry;
                
                // Artworks
                jimport('joomla.filesystem.folder');
                $this->_item->artworks = array();
                $artwork_dir = $this->_item->params->get('artwork_dir');
                if ($artwork_dir) {
                    $path = JPATH_SITE . '/images/' . $artwork_dir;
                    $images = JFolder::files($path, '\.(jpg|png|jpeg|gif)$');
                    foreach ($images as $image)
                        $this->_item->artworks[] = 'images/' . $artwork_dir . '/' . $image;
                }
                
                // Get abilities
                $abilities_model = JModelLegacy::getInstance('Abilities', 'Dota2Model', array('ignore_request' => true));
                $abilities_model->setState('filter.hero', $this->_item->name);
                $abilities_model->setState('list.ordering', 'id');
                $this->_item->abilities = $abilities_model->getItems();
            } elseif ($error = $table->getError()) {
                $this->setError($error);
            }
        }

        return $this->_item;
    }
    
    public function getTable($type = 'Hero', $prefix = 'Dota2Table', $config = array())
    {
        $this->addTablePath(JPATH_ADMINISTRATOR.'/components/com_dota2/tables');
        return JTable::getInstance($type, $prefix, $config);
    }

    
    /**
     * Method to check in an item.
     *
     * @param   integer     The id of the row to check out.
     * @return  boolean     True on success, false on failure.
     * @since   1.6
     */
    public function checkin($id = null)
    {
        // Get the id.
        $id = (!empty($id)) ? $id : (int)$this->getState('hero.id');

        if ($id) {
            
            // Initialise the table
            $table = $this->getTable();

            // Attempt to check the row in.
            if (method_exists($table, 'checkin')) {
                if (!$table->checkin($id)) {
                    $this->setError($table->getError());
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Method to check out an item for editing.
     *
     * @param   integer     The id of the row to check out.
     * @return  boolean     True on success, false on failure.
     * @since   1.6
     */
    public function checkout($id = null)
    {
        // Get the user id.
        $id = (!empty($id)) ? $id : (int)$this->getState('hero.id');

        if ($id) {
            
            // Initialise the table
            $table = $this->getTable();

            // Get the current user object.
            $user = JFactory::getUser();

            // Attempt to check the row out.
            if (method_exists($table, 'checkout')) {
                if (!$table->checkout($user->get('id'), $id)) {
                    $this->setError($table->getError());
                    return false;
                }
            }
        }

        return true;
    }
    
    /**
     * Method to get the profile form.
     *
     * The base form is loaded from XML
     *
     * @param   array   $data       An optional array of data for the form to interogate.
     * @param   boolean $loadData   True if the form is to load its own data (default case), false if not.
     * @return  JForm   A JForm object on success, false on failure
     * @since   1.6
     */
    public function getForm($data = array(), $loadData = true)
    {
        // Get the form.
        $form = $this->loadForm('com_dota2.hero', 'hero', array('control' => 'jform', 'load_data' => $loadData));
        if (empty($form)) {
            return false;
        }

        return $form;
    }

    /**
     * Method to get the data that should be injected in the form.
     *
     * @return  mixed   The data for the form.
     * @since   1.6
     */
    protected function loadFormData()
    {
        $data = $this->getData();
        
        return $data;
    }

    /**
     * Method to save the form data.
     *
     * @param   array       The form data.
     * @return  mixed       The user id on success, false on failure.
     * @since   1.6
     */
    public function save($data)
    {
        $id = (!empty($data['id'])) ? $data['id'] : (int)$this->getState('hero.id');
        $state = (!empty($data['state'])) ? 1 : 0;
        $user = JFactory::getUser();

        if($id) {
            //Check the user can edit this item
            $authorised = $user->authorise('core.edit', 'com_dota2.hero.'.$id) || $authorised = $user->authorise('core.edit.own', 'com_dota2.hero.'.$id);
            if($user->authorise('core.edit.state', 'com_dota2.hero.'.$id) !== true && $state == 1){ //The user cannot edit the state of the item.
                $data['state'] = 0;
            }
        } else {
            //Check the user can create new items in this section
            $authorised = $user->authorise('core.create', 'com_dota2');
            if($user->authorise('core.edit.state', 'com_dota2.hero.'.$id) !== true && $state == 1){ //The user cannot edit the state of the item.
                $data['state'] = 0;
            }
        }

        if ($authorised !== true) {
            JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
            return false;
        }
        
        $table = $this->getTable();
        if ($table->save($data) === true) {
            return $id;
        } else {
            return false;
        }
        
    }
    
     function delete($data)
    {
        $id = (!empty($data['id'])) ? $data['id'] : (int)$this->getState('hero.id');
        if(JFactory::getUser()->authorise('core.delete', 'com_dota2.hero.'.$id) !== true){
            JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
            return false;
        }
        $table = $this->getTable();
        if ($table->delete($data['id']) === true) {
            return $id;
        } else {
            return false;
        }
        
        return true;
    }
    
    function getCategoryName($id){
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query
            ->select('title')
            ->from('#__categories')
            ->where('id = ' . $id);
        $db->setQuery($query);
        return $db->loadObject();
    }
    
}
