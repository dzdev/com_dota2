<?php

/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

include_once JPATH_SITE.'/components/com_dota2/helpers/route.php';
/**
 * Methods supporting a list of Dota2 records.
 */
class Dota2ModelItems extends JModelList {

    /**
     * Constructor.
     *
     * @param    array    An optional associative array of configuration settings.
     * @see        JController
     * @since    1.6
     */
    public function __construct($config = array()) {

        if (empty($config['filter_fields']))
        {
            $config['filter_fields'] = array(
                'id', 'a.id',
                'checked_out', 'a.checked_out',
                'checked_out_time', 'a.checked_out_time',
                'state', 'a.state',
                'created', 'a.created',
                'created_by', 'a.created_by',
                'ordering', 'a.ordering',
                'name', 'a.name',
                'dname', 'a.dname',
                'cost', 'a.cost',
                'dota_id', 'a.dota_id',
                'mc', 'a.mc'
            );
        }

        parent::__construct($config);
    }

    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @since   1.6
     */
    protected function populateState($ordering = null, $direction = null) {

        // Initialise variables.
        $app = JFactory::getApplication();

        // Load the filter state.
        $search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);

        // List state information
        $limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'));
        $this->setState('list.limit', $limit);

        $limitstart = JFactory::getApplication()->input->getInt('limitstart', 0);
        $this->setState('list.start', $limitstart);

        $orderCol = $app->input->get('filter_order', 'a.dname');
        if (!in_array($orderCol, $this->filter_fields))
        {
            $orderCol = 'a.dname';
        }
        $this->setState('list.ordering', $orderCol);

        $listOrder = $app->input->get('filter_order_Dir', 'ASC');
        if (!in_array(strtoupper($listOrder), array('ASC', 'DESC', '')))
        {
            $listOrder = 'ASC';
        }

        $component = $app->input->get('filter_component', '');
        if ($component)
            $this->setState('filter.component', $component);

        $quality = $app->input->get('filter_quality', '');
        if ($quality)
            $this->setState('filter.quality', $quality);

        //Filtering created
        $this->setState('filter.created_by_components', $app->getUserStateFromRequest($this->context.'.filter.created_by_components', 'filter_created_by_components', '', 'string'));

        $this->setState('list.direction', $listOrder);

        // List state information.
        parent::populateState($ordering, $direction);
    }

    /**
     * Build an SQL query to load the list data.
     *
     * @return  JDatabaseQuery
     * @since   1.6
     */
    protected function getListQuery() {
        // Create a new query object.
        $db = $this->getDbo();
        $query = $db->getQuery(true);

        // Select the required fields from the table.
        $query->select(
                $this->getState(
                        'list.select', 'a.*'
                )
        );

        $query->from('`#__dota2_items` AS a');


    // Join over the users for the checked out user.
    $query->select('uc.name AS editor');
    $query->join('LEFT', '#__users AS uc ON uc.id=a.checked_out');

        // Join over the created by field 'created_by'
        $query->select('created_by.name AS created_by');
        $query->join('LEFT', '#__users AS created_by ON created_by.id = a.created_by');
        // Join over the foreign key 'components'
        $query->select('#__dota2_items_607633.dname AS items_dname_607633');
        $query->join('LEFT', '#__dota2_items AS #__dota2_items_607633 ON #__dota2_items_607633.name = a.components');


        // Filter by search in title
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            if (stripos($search, 'id:') === 0) {
                $query->where('a.id = ' . (int) substr($search, 3));
            } else {
                $search = $db->Quote('%' . $db->escape($search, true) . '%');
                $query->where('( a.name LIKE '.$search.'  OR  a.dname LIKE '.$search.'  OR  a.dota_id LIKE '.$search.'  OR  a.lore LIKE '.$search.' )');
            }
        }



        //Filtering components
        $filter_component = $this->state->get("filter.component");
        if ($filter_component) {
            $query->where("a.components REGEXP '(^$filter_component,|,$filter_component,|,$filter_component$|^$filter_component$)'");
        }

        //Filtering created
        $filter_created = $this->state->get("filter.created_by_components");
        if ($filter_created != '') {
            $query->where("a.created = '".$db->escape($filter_created)."'");
        }

        //Filtering qual
        $filter_qual = $this->state->get("filter.quality");
        if ($filter_qual) {
            $query->where("a.qual = '".$filter_qual."'");
        }

        // only show published item
        $query->where('a.state = 1');

        // Add list ordering
        $query->order($this->getState('list.ordering', 'a.dname') . ' ' . $this->getState('list.direction', 'ASC'));
        return $query;
    }

    public function getItems() {
        $items = parent::getItems();

        foreach ($items as &$item) {
            $item->image['dota2']['lg'] = JUri::root().'media/com_dota2/images/dota2/items/'.$item->name.'_lg.png';
            $item->image['dota1']['lg'] = JUri::root().'media/com_dota2/images/dota1/items/'.$item->name.'_lg.png';
            $item->link = JRoute::_(Dota2HelperRoute::getItemRoute($item->id));
            $item->params = new JRegistry($item->params);
        }
        return $items;
    }
}
