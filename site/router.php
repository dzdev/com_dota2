<?php
/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */

// No direct access
defined('_JEXEC') or die;

/**
 * @param   array   A named array
 * @return  array
 */
function Dota2BuildRoute(&$query)
{
    $segments = array();

    // get a menu item based on Itemid or currently active
    $app = JFactory::getApplication();
    $menu = $app->getMenu();

    // we need a menu item.  Either the one specified in the query, or the current active one if none specified
    if (empty($query['Itemid']))
    {
        $menuItem = $menu->getActive();
        $menuItemGiven = false;
    }
    else
    {
        $menuItem = $menu->getItem($query['Itemid']);
        $menuItemGiven = true;
    }

    // check again
    if ($menuItemGiven && isset($menuItem) && $menuItem->component != 'com_dota2')
    {
        $menuItemGiven = false;
        unset($query['Itemid']);
    }

    if (isset($query['view']))
    {
        $view = $query['view'];
    }
    else
    {
        // we need to have a view in the query or it is an invalid URL
        return $segments;
    }
    
    if ($menuItem instanceof stdClass) { 
        // are we dealing with an item that is attached to a menu item?
        if($menuItem->query['view'] == $query['view'] && isset($query['id']) && $menuItem->query['id'] == (int) $query['id'])
        {
            unset($query['view']);

            if (isset($query['layout']))
            {
                unset($query['layout']);
            }

            unset($query['id']);

            return $segments;
        }
        
        // We can imply a single view from its list view
        // Thus we can remove view from the query in some cases
        if ( ($menuItem->query['view'] == 'items' && $query['view'] == 'item') || 
             ($menuItem->query['view'] == 'heroes' && $query['view'] == 'hero') ||
             ($menuItem->query['view'] == 'abilities' && $query['view'] == 'ability'))
            unset($query['view']);
    }
    

    if ($view == 'item' || $view == 'hero' || $view == 'ability')
    {
        if (!$menuItemGiven)
        {
            $segments[] = $view;
            unset($query['view']);
        }

        
        if (isset($query['id']))
        {
            // Make sure we have the id and the alias
            if (strpos($query['id'], ':') === false)
            {
                $db = JFactory::getDbo();
                $dbQuery = $db->getQuery(true)
                    ->select('name')
                    ->where('id=' . (int) $query['id']);
                switch ($view) {
                    case 'item':
                        $dbQuery->from('#__dota2_items');
                        break;
                    case 'hero':
                        $dbQuery->from('#__dota2_heroes');
                        break;
                    case 'ability':
                        $dbQuery->from('#__dota2_abilities');
                        break;
                    default:
                        break;
                }
                $db->setQuery($dbQuery);
                $alias = $db->loadResult();
                $query['id'] = str_replace('_', '-', $alias); // URL should have "-" instead of "_"
            }
        }
        else
        {
            // we should have id set for this view.  If we don't, it is an error
            return $segments;
        }
        
        $segments[] = $query['id'];
        
        unset($query['id']);
    }

    // if the layout is specified and it is the same as the layout in the menu item, we
    // unset it so it doesn't go into the query string.
    if (isset($query['layout']))
    {
        if ($menuItemGiven && isset($menuItem->query['layout']))
        {
            if ($query['layout'] == $menuItem->query['layout'])
            {
                unset($query['layout']);
            }
        }
        else
        {
            if ($query['layout'] == 'default')
            {
                unset($query['layout']);
            }
        }
    }

    return $segments;
}

/**
 * @param   array   A named array
 * @param   array
 *
 * Formats:
 *
 * index.php?/dota2/task/id/Itemid
 *
 * index.php?/dota2/id/Itemid
 */
function Dota2ParseRoute($segments)
{
    $vars = array();
    
    $app = JFactory::getApplication();
    $menu = $app->getMenu();
    $menuItem = $menu->getActive();
    
    // view is always the first element of the array
    $count = count($segments);
    
    if ($count >= 2) {
        $vars['id'] = $segments[$count-1];
        $vars['view'] = $segments[$count-2];
    } elseif ($count == 1) {
        if ($menuItem) {
            switch ($menuItem->query['view']) {
                case 'heroes':
                    $vars['view'] = 'hero';
                    break;
                case 'items':
                    $vars['view'] = 'item';
                    break;
                case 'abilities':
                    $vars['view'] = 'abilities';
                    break;
                default:
                    break;
            }
            $vars['id'] = $segments[0];
        } else {
            $vars['view'] = $segments[0];
        }
    }
    
    if (isset($vars['id'])) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('id');
        
        switch ($vars['view']) {
        case 'hero':
            $query->from('#__dota2_heroes');
            break;
        case 'item':
            $query->from('#__dota2_items');
            break;
        case 'ability':
            $query->from('#__dota2_abilities');
            break;
        default:
            break;
        }
        $vars['id'] = str_replace('-', '_', $vars['id']);
        $vars['id'] = str_replace(':', '_', $vars['id']);
        $query->where('name = \'' . $query->escape(str_replace('-', '_', $vars['id'])) . '\'');
        
        $db->setQuery($query);
        $vars['id'] = $db->loadResult();
    }
    return $vars;
}
