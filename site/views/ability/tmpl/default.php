<?php
/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
// no direct access
defined('_JEXEC') or die;

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_dota2', JPATH_ADMINISTRATOR);
JHtml::_('jquery.framework');
JFactory::getDocument()->addScript(JUri::root() . 'components/com_dota2/assets/javascript/jquery.oembed.js');
JFactory::getDocument()->addScriptDeclaration(<<<EOT
jQuery(document).ready(function(){
    jQuery("a.embed").oembed(null, {embedMethod: "fill"});
});
EOT
);
?>
<?php if ($this->item) : ?>

    <div class="item_fields">

        <ul class="fields_list">

            <li><?php echo JText::_('COM_DOTA2_FORM_LBL_ABILITY_NAME'); ?>:
            <?php echo $this->item->name; ?></li>
            <li><?php echo JText::_('COM_DOTA2_FORM_LBL_ABILITY_DNAME'); ?>:
            <?php echo $this->item->dname; ?></li>
            <li><?php echo JText::_('COM_DOTA2_FORM_LBL_ABILITY_AFFECTS'); ?>:
            <?php echo $this->item->affects; ?></li>
            <li><?php echo JText::_('COM_DOTA2_FORM_LBL_ABILITY_ATTRIB'); ?>:
            <?php echo $this->item->attrib; ?></li>
            <li><?php echo JText::_('COM_DOTA2_FORM_LBL_ABILITY_CMB'); ?>:
            <?php echo $this->item->cmb; ?></li>
            <li><?php echo JText::_('COM_DOTA2_FORM_LBL_ABILITY_DESC'); ?>:
            <?php echo $this->item->desc; ?></li>
            <li><?php echo JText::_('COM_DOTA2_FORM_LBL_ABILITY_DMG'); ?>:
            <?php echo $this->item->dmg; ?></li>
            <li><?php echo JText::_('COM_DOTA2_FORM_LBL_ABILITY_HURL'); ?>:
            <?php echo $this->item->hurl; ?></li>
            <li><?php echo JText::_('COM_DOTA2_FORM_LBL_ABILITY_LORE'); ?>:
            <?php echo $this->item->lore; ?></li>
            <li>Shortcut: <?php echo $this->item->params->get('shortcut'); ?></li>
            <li>Video:
                <a href="<?php echo $this->item->params->get('media'); ?>" class="embed"><?php echo $this->item->params->get('media'); ?></a>
            </li>

        </ul>

    </div>
    
<?php
else:
    echo JText::_('COM_DOTA2_ITEM_NOT_LOADED');
endif;
?>
