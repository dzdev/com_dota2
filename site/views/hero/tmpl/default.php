<?php
/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
// no direct access
defined('_JEXEC') or die;

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_dota2', JPATH_ADMINISTRATOR);
JHtml::_('jquery.framework');
JHtml::_('script', 'com_dota2/jquery.oembed.js', true, true);
JFactory::getDocument()->addScriptDeclaration(<<<EOT
jQuery(document).ready(function(){
    jQuery("a.embed").oembed(null, {embedMethod: "fill"});
});
EOT
);
?>
<?php if ($this->item) : ?>

    <div class="item_fields">

        <ul class="fields_list">
            <li>Name: <?php echo $this->item->dname; ?></li>
            <li>Tags: 
                <?php $this->item->tagLayout = new JLayoutFile('joomla.content.tags'); ?>
                <?php echo $this->item->tagLayout->render($this->item->tags->itemTags); ?>
            </li>
            <li>Video:
                <a href="<?php echo $this->item->params->get('media'); ?>" class="embed"><?php echo $this->item->params->get('media'); ?></a>
            </li>
            <li>Images:
                <ul>
                    <li><img src="<?php echo $this->item->image['dota2']['full']; ?>" /></li>
                    <li><img src="<?php echo $this->item->image['dota2']['hphover']; ?>" /></li>
                    <li><img src="<?php echo $this->item->image['dota2']['sb']; ?>" /></li>
                    <li><img src="<?php echo $this->item->image['dota2']['vert']; ?>" /></li>
                    <li><img src="<?php echo $this->item->image['dota1']['full']; ?>" /></li>
                    <li><img src="<?php echo $this->item->image['dota1']['hphover']; ?>" /></li>
                </ul>
            </li>
            <li>Attack: <?php echo $this->item->atk; ?></li>
            <li>Bio: <?php echo $this->item->bio; ?></li>
            <li>Bio Alt: <?php echo $this->item->params->get('bio_alt'); ?></li>
            <li>Roles: <?php echo $this->item->droles; ?></li>
            <li>Primary Attrib: <?php echo $this->item->pa; ?></li>
            <li>Guide: <?php echo $this->item->params->get('guide'); ?></li>
            <li>Attribs:
            <ul>
                <li>Strength:
                <ul>
                    <li>Base: <?php echo $this->item->attribs->str->b; ?></li>
                    <li>Gain: <?php echo $this->item->attribs->str->g; ?></li>
                </ul>
                </li>
                <li>Intelligence:
                <ul>
                    <li>Base: <?php echo $this->item->attribs->int->b; ?></li>
                    <li>Gain: <?php echo $this->item->attribs->int->g; ?></li>
                </ul>
                </li>
                <li>Agility:
                <ul>
                    <li>Base: <?php echo $this->item->attribs->agi->b; ?></li>
                    <li>Gain: <?php echo $this->item->attribs->agi->g; ?></li>
                </ul>
                </li>
                <li>Move speed: <?php echo $this->item->attribs->ms; ?></li>
                <li>Damage: 
                <ul>
                    <li>Min: <?php echo $this->item->attribs->dmg->min; ?></li>
                    <li>Max: <?php echo $this->item->attribs->dmg->max; ?></li>
                </ul>
                </li>
                <li>Armor: <?php echo $this->item->attribs->armor; ?></li>
                <li>Attack:
                <ul>
                    <li>Range: <?php echo $this->item->attribs->range; ?></li>
                    <li>Base Time: <?php echo $this->item->attribs->base_attack_time; ?></li>
                </ul>
                </li>
                <li>Vision:
                <ul>
                    <li>Day: <?php echo $this->item->attribs->day_vision; ?></li>
                    <li>Night: <?php echo $this->item->attribs->night_vision; ?></li>
                </li>
            </ul>
            </li>
            <li>Rating
                <ul>
                    <li>Rating1: <?php echo $this->item->params->get('rating1'); ?></li>
                    <li>Rating2: <?php echo $this->item->params->get('rating2'); ?></li>
                    <li>Rating3: <?php echo $this->item->params->get('rating3'); ?></li>
                    <li>Rating4: <?php echo $this->item->params->get('rating4'); ?></li>
                </ul>
            </li>
            <li>Artworks: 
                <ul>
                <?php foreach ($this->item->artworks as $artwork) : ?>
                <li><?php echo $artwork; ?></li>
                <?php endforeach; ?>
                </ul>
            </li>
            
            <li>Abilities:
            <ul>
            <?php foreach ($this->item->abilities as $ability) : ?>
            <li><?php echo $ability->dname; ?>:
                <ul>
                    <li>Images:
                        <ul>
                            <li><img src="<?php echo $ability->image['dota2']['hp1']; ?>" /></li>
                            <li><img src="<?php echo $ability->image['dota2']['hp2']; ?>" /></li>
                            <li><img src="<?php echo $ability->image['dota1']['hp1']; ?>" /></li>
                        </ul>
                    </li>
                    <li>Affects: <?php echo $ability->affects; ?></li>
                    <li>Affects alt: <?php echo $ability->params->get('affects_alt'); ?></li>
                    <li>Attrib: <?php echo $ability->attrib; ?></li>
                    <li>Attrib alt: <?php echo $ability->params->get('attrib_alt'); ?></li>
                    <li>Cooldown / Mana Cost: <?php echo $ability->cmb; ?></li>
                    <li>Cooldown alt: <?php echo $ability->params->get('cmb_alt'); ?></li>
                    <li>Description: <?php echo $ability->desc; ?></li>
                    <li>Description alt: <?php echo $ability->params->get('desc_alt'); ?></li>
                    <li>Damage: <?php echo $ability->dmg; ?></li>
                    <li>Damage alt: <?php echo $ability->params->get('dmg_alt'); ?></li>
                    <li>Hero URL suffix: <?php echo $ability->hurl; ?></li>
                    <li>Lore: <?php echo $ability->lore; ?></li>
                    <li>Lore alt: <?php echo $ability->params->get('lore_alt'); ?></li>
                    <li>Shortcut: <?php echo $ability->params->get('shortcut'); ?></li>
                    <li>Video:
                        <a href="<?php echo $ability->params->get('media'); ?>" class="embed"><?php echo $ability->params->get('media'); ?></a>
                    </li>
                </ul>
            </li>
            <?php endforeach; ?>
            </ul>
            </li>
        </ul>
    </div>
    
<?php
else:
    echo JText::_('COM_DOTA2_ITEM_NOT_LOADED');
endif;
?>
