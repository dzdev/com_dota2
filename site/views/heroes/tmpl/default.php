<?php
/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('bootstrap.framework');
$document = JFactory::getDocument();

?>

<?php
    // Categorizing heroes
    $heroes = array(
        "radiant" => array(
            "str" => array(),
            "agi" => array(),
            "int" => array(),
        ),
        "dire" => array(
            "str" => array(),
            "agi" => array(),
            "int" => array(),
        ),
    );
    foreach ($this->items as $item) {
        $heroes[$item->faction][$item->pa][] = $item;
    }
?>
<script>
  jQuery(window).load(function() {
  jQuery('#load').show()
  jQuery('#loading').hide()

});
</script>

<div class="dota2-heroes">
    <h1 class="big-heading">Tướng Dota 2</h1>
    <div class="inner" id="load" style="display:none;">
    <?php foreach ($heroes as $faction => $faction_heroes) : ?>
    <div class="row">
      <?php foreach ($faction_heroes as $pa => $pa_heroes) : ?>
        <div class="col-md-4">
          <div class="row hero-group">
              <h4 align="center">
      <?php switch ($pa) {
          case 'str': ?>
            <img src="<?= JUri::root() ?>components/com_dota2/assets/images/icon-<?php echo $pa;?>.png" alt="Strength"/><br/>Tướng Strength
            <?php break;
          case 'agi':?>
            <img src="<?= JUri::root() ?>components/com_dota2/assets/images/icon-<?php echo $pa;?>.png" alt="Agility"/><br/>Tướng Agility
            <?php break;
          case 'int':?>
            <img src="<?= JUri::root() ?>components/com_dota2/assets/images/icon-<?php echo $pa;?>.png" alt="Intelligence"/><br />Tướng Intelligence
            <?php break;
      }?>
      </h4>
            <?php foreach ($pa_heroes as $hero) : ?>
                <div class="hero-cell col-xs-3 <?php echo $hero->filter_class; ?>">
                    <a href="<?php echo $hero->link;?>" title="<?php echo $hero->dname; ?>">

                          <img src="<?php echo $hero->image['dota2']['full']; ?>" alt="<?php echo $hero->dname; ?>" class="hero-icon" onmouseover="this.src='<?php echo $hero->image['dota1']['full']; ?>'" onmouseout="this.src='<?php echo $hero->image['dota2']['full']; ?>'"/>


                    </a>
                </div>
            <?php endforeach; ?>
            </div>
        </div>
      <?php endforeach; ?>
      </div>
    <?php endforeach; ?>
  </div>
    <div id="loading" align="center"><div class="inner" style="text-decoration:blink">Loading...</div></div>
</div>
<style type="text/css">
.hero-group, .item-group {padding:0px 10px; margin:0px -2px; margin-bottom:30px;}
.hero-group h4, .item-group h4 {text-align:center; font-size:13px; line-height:20px; margin-bottom:20px; font-family:"Roboto Slab"; text-transform:uppercase; color:#666; font-weight:normal; letter-spacing:2px;}
.hero-group h4 img, .item-group h4 img {height:30px; width:30px;}
.hero-cell a, .item-cell a {display:block; text-align:center !important; color:#999;  }
.hero-cell, .item-cell {padding:0px 2px; margin-bottom:10px; position:relative;}
.hero-cell img , .item-cell img {width:60px; height:auto;}
.hero-cell a {height:36px;}
img.hero-icon, img.item-icon {border:3px solid #333; border-radius:3px;}
.hero-cell a {height:36px;}
.hero-cell a:hover img {position:relative; top:-12px; z-index:999; box-shadow:0px 0px 30px #ffcc33}
</style>