<?php
/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */
// no direct access
defined('_JEXEC') or die;

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_dota2', JPATH_ADMINISTRATOR);

?>
<?php if ($this->item) : ?>

    <div class="item_fields">

        <ul class="fields_list">
            <li>Images:
                <ul>
                    <li><img src="<?php echo $this->item->image['dota2']['lg']; ?>" /></li>
                    <li><img src="<?php echo $this->item->image['dota1']['lg']; ?>" /></li>
                </ul>
            </li>
            <li>Attrib: <?php echo $this->item->attrib; ?></li>
            <li>Attrib alt: <?php echo $this->item->params->get('attrib_alt'); ?></li>
            <li>Cooldown: <?php echo $this->item->cd; ?></li>
            <li>Name: <?php echo $this->item->dname; ?></li>
            <li>Components:
                <?php foreach($this->item->components as $component) : ?>
                <a href="<?php echo $component->link; ?>"><?php echo $component->dname; ?></a>
                <?php endforeach; ?>
            </li>
            <li>Part of:
                <?php foreach($this->item->part_of as $item) : ?>
                <a href="<?php echo $item->link; ?>"><?php echo $item->dname; ?></a>
                <?php endforeach; ?>
            </li>
            <li>Cost: <?php echo $this->item->cost; ?></li>
            <li>Recipe Cost: <?php echo $this->item->params->get('recipe_price', 0); ?></li>
            <li>Created by components: <?php echo $this->item->created; ?></li>
            <li>Description: <?php echo $this->item->desc; ?></li>
            <li>Description alt: <?php echo $this->item->params->get('desc_alt'); ?></li>
            <li>Dota ID: <?php echo $this->item->dota_id; ?></li>
            <li>Lore: <?php echo $this->item->lore; ?></li>
            <li>Lore alt: <?php echo $this->item->params->get('lore_alt'); ?></li>
            <li>Mana cost: <?php echo $this->item->mc; ?></li>
            <li>Quality: <?php echo $this->item->qual; ?></li>

        </ul>

    </div>

<?php
else:
    echo JText::_('COM_DOTA2_ITEM_NOT_LOADED');
endif;
?>
