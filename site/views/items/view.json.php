<?php
/**
 * @version     1.0.0
 * @package     com_dota2
 * @copyright   Copyright (C) 2013. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      DZ Team <support@dezign.vn> - dezign.vn
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Dota2.
 */
class Dota2ViewItems extends JViewLegacy
{
    protected $items;

    /**
     * Display the view
     */
    public function display($tpl = null)
    {
        $this->items        = $this->get('Items');

        JFactory::getDocument()->setMimeEncoding( 'application/json' );
        // Check for errors.
        if (count($errors = $this->get('Errors'))) {;
            echo json_encode(array('errors' => $errors));
        } else {
            echo json_encode(array('data' => $this->items));
        }
        JFactory::getApplication()->close();
    }
}
